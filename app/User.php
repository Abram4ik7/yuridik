<?php

namespace App;

use App\User;
use Illuminate\Notifications\Notifiable;
use Illuminate\Database\Eloquent\Relations\BelongsTo;
use Illuminate\Foundation\Auth\User as Authenticatable;

class User extends Authenticatable
{
    use Notifiable;

    protected $fillable = [
        'first_name', 'last_name', 'phone', 'email', 'photo', 'lang', 'role_id', 'city_id', 'verify', 'verify_code', 'thanks', 'ip',
    ];

    protected $hidden = [
        'remember_token',
    ];

    protected $casts = [
        'first_name' => 'string',
        'last_name' => 'string',
        'phone' => 'integer',
        'email' => 'string',
        'photo' => 'string',
        'lang' => 'array',
        'verify' => 'boolean',
        'verify_code' => 'integer',
        'thanks' => 'integer',
        'ip' => 'string',
        'created_at' => 'datetime:d.m.Y, H:i',
        'updated_at' => 'datetime:d.m.Y, H:i',
    ];

    public function id(): int
    {
        return (int) $this->id;
    }

    public function firstName(): string
    {
        return (string) $this->first_name;
    }

    public function lastName(): string
    {
        return (string) $this->last_name;
    }

    public function fullName(): string
    {
        return (string) $this->first_name . ' ' . $this->last_name;
    }

    public function email(): string
    {
        return (string) $this->email;
    }

    public function photo(): string
    {
        return (string) $this->photo;
    }

    public function lang(): array
    {
        return (array) $this->lang;
    }

    public function role(): BelongsTo
    {
        return $this->belongsTo(Role::class, 'role_id');
    }

    public function city(): BelongsTo
    {
        return $this->belongsTo(City::class, 'city_id');
    }

    public function ip(): string
    {
        return (string) $this->ip;
    }

    public function hasPermission(string $permission): bool
    {
        return $this->role->hasPermission($permission);
    }

    public function isAdmin(): bool
    {
        return $this->role_id == Role::ADMIN;
    }

    public function isModer(): bool
    {
        return $this->role_id == Role::MODER;
    }

    public function isAdvocacy(): bool
    {
        return $this->role_id == Role::ADVOCACY;
    }

    public function isLawyer(): bool
    {
        return $this->role_id == Role::LAWYER;
    }

    public function isLegalCounsel(): bool
    {
        return $this->role_id == Role::LEGAL_COUNSEL;
    }

    public function isLegalPerson(): bool
    {
        return $this->role_id == Role::LEGAL_PERSON;
    }

    public function isClient(): bool
    {
        return $this->role_id == Role::CLIENT;
    }

    public function isVerify(): bool
    {
        return $this->verify;
    }

    public function isNotVerify(): bool
    {
        return ! $this->isVerify();
    }

    public function isVerifyCode(string $code): bool
    {
        return $this->verify_code == $code;
    }

    public function isNotVerifyCode(string $code): bool
    {
        return ! $this->isVerifyCode($code);
    }

    public static function findByPhone(int $phone): self
    {
        return self::where('phone', $phone)->firstOrFail();
    }

    public function isReply(): bool
    {
        if($this->isAdmin() || $this->isModer() || $this->isLawyer() || $this->isAdvocacy())  {
            return true;
        }else {
            return false;
        }
    }

    public function isViewQuestions(): bool
    {
        if($this->isAdmin() || $this->isModer() || $this->isLawyer() || $this->isAdvocacy() || $this->isLegalCounsel())  {
            return true;
        }else {
            return false;
        }
    }

    static function randLawyers()
    {
        return User::whereIn('role_id',[4,5])
            ->limit(6)->inRandomOrder()
            ->get();
    }


}
