<?php

namespace App\Jobs\Auth;

use App\User;

class VerifyJob
{
    protected $user;

    public function __construct(User $user)
    {
        $this->user = $user;
    }

    public function handle(): User
    {
        $this->user->verify = true;
        $this->user->verify_code = null;
        $this->user->save();

        return $this->user;
    }
}
