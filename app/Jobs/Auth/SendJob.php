<?php

namespace App\Jobs\Auth;

use App\User;
use App\Api\Sms;

class SendJob
{
    protected $user;
    protected $sms;

    public function __construct(User $user)
    {
        $this->user = $user;
        $this->sms = new Sms();
    }

    public function handle(): User
    {
        $this->user->verify = false;
        $this->user->verify_code = rand(10000, 99999);
        $this->user->save();

        $this->sms->send($this->user->phone, 'Your verify code: ' . $this->user->verify_code);

        return $this->user;
    }
}
