<?php

namespace App\Jobs\Auth;

use App\User;
use App\Http\Requests\Auth\RegisterRequest;

class RegisterJob
{
    protected $attributes;

    public function __construct(array $attributes = [])
    {
        $this->attributes = array_only($attributes, ['first_name', 'last_name', 'phone', 'email', 'lang', 'role_id', 'city_id', 'ip']);
    }

    public static function fromRequest(RegisterRequest $request): self
    {
        return new static([
            'first_name' => $request->firstName(),
            'last_name' => $request->lastName(),
            'phone' => $request->phone(),
            'email' => $request->email(),
            'lang' => $request->lang(),
            'role_id' => $request->role()->id(),
            'city_id' => $request->city()->id(),
            'ip' => $request->ip()
        ]);
    }

    public function handle(): User
    {
        $user = User::create($this->attributes);

        return $user;
    }
}
