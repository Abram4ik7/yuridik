<?php

namespace App\Jobs\Admin;

use App\User;

class UserDeleteJob
{
    protected $user;

    public function __construct(User $user)
    {
        $this->user = $user;
    }

    public function handle()
    {
        $this->user->delete();
    }
}
