<?php

namespace App\Jobs\Admin;

use App\User;
use App\Http\Requests\Admin\UserUpdateRequest;

class UserUpdateJob
{
    protected $user;
    protected $attributes;

    public function __construct(User $user, array $attributes = [])
    {
        $this->user = $user;
        $this->attributes = array_only($attributes, ['first_name', 'last_name', 'email', 'photo', 'role_id', 'city_id']);
    }

    public static function fromRequest(User $user, UserUpdateRequest $request): self
    {
        return new static($user, [
            'first_name' => $request->firstName(),
            'last_name' => $request->lastName(),
            'email' => $request->email(),
            'photo' => $request->photo(),
            'role_id' => $request->role()->id(),
            'city_id' => $request->city()->id(),
        ]);
    }

    public function handle(): User
    {
        $this->user->update($this->attributes);

        return $this->user;
    }
}
