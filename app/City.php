<?php

namespace App;

use Illuminate\Support\Facades\App;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\HasMany;

class City extends Model
{
    protected $fillable = [
        'slug', 'name',
    ];

    protected $casts = [
        'slug' => 'string',
        'name' => 'array',
    ];

    const NO_REGION = 1;
    const TASHKENT_CITY = 2;
    const TASHKENT_REGION = 3;
    const ANDIJAN = 4;
    const BUKHARA = 5;
    const JIZZAKH = 6;
    const KARAKALPAKSTAN = 7;
    const KASHKADARYA = 8;
    const NAVOI = 9;
    const NAMANGAN = 10;
    const SAMARKAND = 11;
    const SURKHANDARYA = 12;
    const SYRDARYA = 13;
    const FERGANA = 14;
    const KHOREZM = 15;

    public function id(): int
    {
        return (int) $this->id;
    }

    public function slug(): string
    {
        return (string) $this->slug;
    }

    public function name(): string
    {
        return App::isLocale('uz') ? $this->name['uz'] : $this->name['ru'];
    }

    public function users(): HasMany
    {
        return $this->hasMany(User::class, 'city_id');
    }

    public static function findBySlug(string $slug): self
    {
        return static::where('slug', $slug)->firstOrFail();
    }
}
