<?php

namespace App\Rules\Auth;

use Illuminate\Contracts\Validation\Rule;

class RoleRule implements Rule
{
    public function passes($attribute, $value): bool
    {
        return preg_match('/(?:legal-person|client)/', $value);
    }

    public function message(): string
    {
        return 'The validation error message.';
    }
}
