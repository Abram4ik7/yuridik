<?php

namespace App\Rules\Auth;

use Illuminate\Contracts\Validation\Rule;

class PhoneRule implements Rule
{
    public function passes($attribute, $value): bool
    {
        return preg_match('/^\d{9}$/', $value);
    }

    public function message(): string
    {
        return 'The validation error message.';
    }
}
