<?php

namespace App\Http\Controllers\Admin;

use App\User;

use App\Jobs\Admin\UserUpdateJob;
use App\Jobs\Admin\UserDeleteJob;

use App\Http\Controllers\Controller;
use App\Http\Requests\Admin\UserUpdateRequest;

class UsersController extends Controller
{
    public function index()
    {
        return view('admin.users');
    }

    public function update(UserUpdateRequest $request, User $user)
    {
        $this->authorize('update', $user);

        $this->dispatchNow(UserUpdateJob::fromRequest($user, $request));

        return response()->json([
            'update' => true,
        ]);
    }

    public function delete(User $user)
    {
        $this->authorize('delete', $user);

        $this->dispatchNow(new UserDeleteJob($user));

        return response()->json([
            'delete' => true,
        ]);
    }
}
