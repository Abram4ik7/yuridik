<?php

namespace App\Http\Controllers;

use App\User;
use Exception;

use App\Jobs\Auth\SendJob;
use App\Jobs\Auth\VerifyJob;
use App\Jobs\Auth\RegisterJob;

use App\Http\Requests\Auth\LoginRequest;
use App\Http\Requests\Auth\RegisterRequest;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

class AuthController extends Controller
{
    public function __construct()
    {
        $this->middleware('guest')->except('logout');
    }

    public function index()
    {
        return view('auth.index');
    }

    public function login(LoginRequest $request)
    {
        try {
            $user = User::findByPhone($request->phone());
        } catch (Exception $exception) {
            return response()->json([
                'user' => false,
            ]);
        }

        $this->dispatch(new SendJob($user));

        return response()->json([
            'send' => true,
        ]);
    }

    public function register(RegisterRequest $request)
    {
        $user = $this->dispatchNow(RegisterJob::fromRequest($request));

        $this->dispatch(new SendJob($user));

        return response()->json([
            'send' => true,
        ]);
    }

    public function verify(User $user, $code)
    {
        if ($user->isNotVerifyCode($code)) {
            return response()->json([
                'code' => false,
            ]);
        }

        $this->dispatchNow(new VerifyJob($user));

        $this->guard()->login($user, true);

        return response()->json([
            'verify' => true,
        ]);
    }

    public function send(User $user)
    {
        if ($user->isVerify()) {
            return response()->json([
                'verify' => true,
            ]);
        }

        $this->dispatch(new SendJob($user));

        return response()->json([
            'send' => true,
        ]);
    }

    public function logout(Request $request)
    {
        $this->guard()->logout();

        $request->session()->invalidate();

        return response()->json([
            'logout' => true,
        ]);
    }

    protected function guard()
    {
        return Auth::guard();
    }
}
