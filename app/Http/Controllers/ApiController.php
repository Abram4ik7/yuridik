<?php

namespace App\Http\Controllers;

use App\User;

class ApiController extends Controller
{
    protected $user;

    public function __construct(User $user)
    {
        $this->user = $user;
    }

    public function users()
    {
        $users = $this->user->with('role', 'city')->latest('id')->get();

        return response()->json($users);
    }
}
