<?php

namespace App\Http\Controllers\Site;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Models\File;

class DownloadCtrl extends Controller
{
    public function download($id)
    {
        $file = File::find($id);

        if(!empty($file)) {
             return response()->download($file->path, $file->name);
        }

        return abort(404);
    }
}
