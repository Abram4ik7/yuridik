<?php

namespace App\Http\Controllers\Site;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Models\Blog;
use App;

class PostsCtrl extends Controller
{
    public function index()
    {
        $posts = Blog::latest('id','desc')->where('published',1)->where('lang', App::getlocale() )->paginate('9');
        return view('site.posts.list',compact('posts'));
    }

    public function view($id, $slug, Request $request)
    {
        $data = Blog::find($id);

        if(!empty($data) && $data->published == 1) {
            $data->views += 1;
            $data->save();

            $posts = Blog::where('lang', App::getlocale() )->inRandomOrder()->where('published',1)->limit(3)->get();

            return view('site.posts.view',compact('data','posts'));
        }

        return abort('404');
    }
}
