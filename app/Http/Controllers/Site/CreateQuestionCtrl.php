<?php

namespace App\Http\Controllers\Site;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use User;
use App\Models\File;
use App\Models\Question;
use App\Models\Answer;

class CreateQuestionCtrl extends Controller
{
    public function create()
    {
        return view('site.create.questions.index');
    }
}
