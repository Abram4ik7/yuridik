<?php

namespace App\Http\Controllers\Site;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\User;

class LawyersCtrl extends Controller
{
    public function index()
    {
        $lawyers = User::latest('thanks','desc')->whereIn('role_id',[4,5])->paginate(12);
        $randLawyers = User::randLawyers();
        return view('site.lawyers.list',compact('lawyers','randLawyers'));
    }

    public function thanks($id, Request $request)
    {
        $request->session()->put('gratitude' . $id, '1');

        $user = User::find($id);
        $user->thanks += 1;
        $user->save();

        return response()->json(['status' => true, 'count' => $user->thanks]);
    }
}
