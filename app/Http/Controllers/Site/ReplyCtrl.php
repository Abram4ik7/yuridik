<?php

namespace App\Http\Controllers\Site;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\User;
use App\Models\Question;
use App\Models\Answer;
use App\Queries\Questions;
use App\Queries\Answers;
use App\Http\Requests\StoreReply;
use Illuminate\Support\Facades\Validator;
use DB;
use App;

class ReplyCtrl extends Controller
{
    public function reply_ajax(Request $request)
    {
        $id = $request->id;
        $text = $request->text;

        $question = Question::find($id);

        if(auth()->check() && $question->service_id == 3 && auth()->user()->isLegalCounsel() || auth()->check() && auth()->user()->isReply() ) { // agar bepul savol bolsa oddiy huquqshunos javob bera oladi //agar bepul savol bolasa advokatligini tekshiramiz

            $answer = Answers::ReplyCreate($id,$text);

            $data = [
                'status' => true
            ];
        } else {
            $data = [
                'status' => false,
                'message' => 'You have no right!'
            ];
        }

        return $data;
    }

    public function reply($id, Request $request)
    {
        $validator = Validator::make($request->all(), [
            //'files.*' => 'file|mimes:jpg,jpeg,bmp,png,pdf,gif,doc,docs,rtf,odt,txt,xls,xlsx|size:20480',
            'full_text' => 'required',
        ]);


        if ($validator->fails()) {
            $data = [
                'status' => false,
                'errors' => $validator->errors()
            ];

        } else {
            $answer = Answers::CreateInQuestion($id,$request);
            $data = [
                'status' => true,
                'id' => $id,
                'answer_id' => $answer->id,
                'message' => trans('site.questions.success_answered'),
                'view' => view('site.questions.answersTemplate.answersAjax',compact('answer'))->render()
            ];
        }

        return response()->json($data);

    }


    public function additional($id, Request $request)
    {
        $validator = Validator::make($request->all(), [
            //'files.*' => 'file|mimes:jpg,jpeg,bmp,png,pdf,gif,doc,docs,rtf,odt,txt,xls,xlsx|size:20480',
            'full_text' => 'required',
        ]);


        if ($validator->fails()) {
            $data = [
                'status' => false,
                'errors' => $validator->errors()
            ];

        } else {
            $question = Question::find($id);
            if(auth()->check() && $question->user_id == auth()->user()->id) {
                $q = Questions::CreateAdditionals($id, $request, $question);
                $data = [
                    'status' => true,
                    'id' => $id,
                    'view' => view('site.questions.additional_ajax', compact('q'))->render()
                ];
            } else {
                return abort(500);
            }


        }

        return response()->json($data);
    }

    public function additional_answer($id, Request $request)
    {
        $validator = Validator::make($request->all(), [
            //'files.*' => 'file|mimes:jpg,jpeg,bmp,png,pdf,gif,doc,docs,rtf,odt,txt,xls,xlsx|size:20480',
            'text' => 'required',
        ]);


        if ($validator->fails()) {
            $data = [
                'status' => false,
                'id' => $id,
                'errors' => $validator->errors()
            ];

        } else {
            $question = Answer::find($id);
            if(auth()->check()) {
                $answer = Answers::CreateAdditionalAnswers($id, $request);
                $data = [
                    'status' => true,
                    'id' => $id,
                    'view' => view('site.questions.answersTemplate.subAnswerAjax', compact('answer'))->render()
                ];
            } else {
                return abort(500);
            }


        }

        return response()->json($data);
    }

}
