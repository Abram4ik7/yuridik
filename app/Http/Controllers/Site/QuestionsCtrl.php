<?php

namespace App\Http\Controllers\Site;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\User;
use App\Models\Question;
use App\Models\Answer;
use App\Queries\Questions;
use App\Queries\Answers;
use DB;
use App;


class QuestionsCtrl extends Controller
{

    private function randLawyers()
    {
        return User::whereIn('role_id',[4,5])
            ->limit(6)->inRandomOrder()
            ->get();
    }

    public function list()
    {
        $questions = Questions::listAllSql();
        $randLawyers =  User::randLawyers();
        return view('site.questions.list', compact('randLawyers', 'questions'));
    }

    public function free()
    {
        $questions = Questions::listFreeSql();
        $randLawyers =  User::randLawyers();

        return view('site.questions.list', compact('randLawyers', 'questions'));
    }

    public function paid()
    {
        $questions = Questions::listPaidSql();
        $randLawyers =  User::randLawyers();

        return view('site.questions.list', compact('randLawyers', 'questions'));
    }


    public function view($id, $slug, Request $request)
    {
        $question = Question::find($id);

        if(empty($question)) {
            abort(404);
        }


        if(auth()->guest() || auth()->check() && !auth()->user()->isReply()) {
            if($question->private_question) {
                $request->session()->flash('msg'.$id, '1');
                return redirect()->back();
            }
        }


        $answers = Questions::answers_sql($id, $question->service_id, $question->user_id);

        return view('site.questions.view', compact('question','answers'));
    }





}
