<?php

namespace App\Http\Controllers\Dashboard;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Models\Setting;

class SettingsCtrl extends Controller
{
    public function index()
    {
        $data = Setting::find(1);
        return view('dashboard.settings',compact('data'));
    }

    public function update(Request $req)
    {
        $data = Setting::find(1);
        $data->title = ["uz" => $req->title_uz, "ru" => $req->title_ru];
        $data->address = ['uz' => $req->address_uz, 'ru' => $req->address_ru];
        $data->keywords = ['uz' => $req->keywords_uz, 'ru' => $req->keywords_ru];
        $data->description = ['uz' => $req->description_uz, 'ru' => $req->description_ru];
        $data->phone = $req->phone;
        $data->email = $req->email;
        $data->facebook_link = $req->facebook_link;
        $data->instagram_link = $req->instagram_link;
        $data->telegram_link = $req->telegram_link;
        $data->commission = $req->commission;
        $data->save();

        $req->session()->flash('info',trans('admin.successfull_edited'));
        return redirect()->route('d-settings');
    }

    public function json($data)
    {
        return json_encode($data);
    }
}
