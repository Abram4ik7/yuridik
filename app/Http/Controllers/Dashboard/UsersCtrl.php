<?php

namespace App\Http\Controllers\Dashboard;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\User;
use App\City;
use App\Role;


class UsersCtrl extends Controller
{
    public function index()
    {
        $users = User::latest('id','desc')->paginate(20);
        return view('dashboard.users.list',compact('users'));
    }

    public function search(Request $request)
    {
        $users = User::where('id',$request->get('q'))->orWhere('phone','like',"%{$request->get('q')}%")->paginate(100);
        return view('dashboard.users.list',compact('users'));
    }

    public function delete($id, Request $request)
    {
        $data = User::find($id);
        if(!empty($data)){
            if(is_file($data->photo)){
                unlink($data->photo);
            }
            $data->delete();
            $request->session()->flash('info',trans('admin.successful_delete'));
        }else{
            $request->session()->flash('info',trans('admin.not_found_item'));
        }

        return redirect()->back();
    }


    public function edit($id, Request $request)
    {
        $data = User::find($id);
        if(!empty($data)){
            $roles = Role::all();
            $citys  = City::all();
            return view('dashboard.users.edit',compact('data','roles','citys'));
        }

        $request->session()->flash('info',trans('admin.not_found_item'));
    }


    public function update($id, Request $request)
    {
        $data = User::find($id);
        $data->first_name = $request->firstname;
        $data->last_name = $request->lastname;
        $data->city_id = $request->city_id;
        $data->role_id = $request->role_id;
        $data->phone = $request->phone;
        if($request->photo) {
            if(is_file($data->photo)) {
                unlink($data->photo);
            }

            $date = date('Y',time()).'/'.date('m',time());
            $path = $request->photo->store("uploads/users/{$date}/");

            $data->photo = $path;
        }

        $data->save();

        $request->session()->flash('info',trans('admin.successfull_edited'));
        return redirect()->route('d-users');
    }
}
