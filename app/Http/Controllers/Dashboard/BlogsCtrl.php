<?php

namespace App\Http\Controllers\Dashboard;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Models\Blog;


class BlogsCtrl extends Controller
{
    public function index()
    {
        $blogs = Blog::latest('id','desc')->paginate(30);
        return view('dashboard.blogs.list',compact('blogs'));
    }


    public function delete($id, Request $request)
    {
        $data = Blog::find($id);
        if(!empty($data)){
            if(is_file($data->poster)){
                unlink($data->poster);
            }
            $data->delete();
            $request->session()->flash('info',trans('admin.successful_delete'));
        }else{
            $request->session()->flash('info',trans('admin.not_found_item'));
        }

        return redirect()->back();
    }


    public function add()
    {
        return view('dashboard.blogs.add');
    }

    public function edit($id, Request $request)
    {
        $data = Blog::find($id);
        if(!empty($data)){
            return view('dashboard.blogs.edit',compact('data'));
        }

        $request->session()->flash('info',trans('admin.not_found_post'));
        return redirect()->back();
    }


    public function create(Request $request)
    {
        $blog = new Blog;
        $blog->title = $request->title;
        $blog->slug = str_slug($request->title);
        if($request->poster) {
            $date = date('Y',time()).'/'.date('m',time());
            $path = $request->poster->store("uploads/blogs/{$date}/");
            $blog->poster = $path;
        }
        $blog->user_id = auth()->user()->id;
        $blog->cat_id = $request->cat_id;
        $blog->full_text = $request->full_text;
        if($request->published){
            $blog->published = 1;
        }else{
            $blog->published = 0;
        }
        $blog->save();

        $request->session()->flash('info',trans('admin.successfull_added'));
        return redirect()->route('d-blogs');
    }

    public function update($id, Request $request)
    {
        $blog = Blog::find($id);
        if (!empty($blog)){
            $blog->title = $request->title;
            $blog->slug = str_slug($request->title);
            if($request->poster) {

                if(is_file($blog->poster)) {
                    unlink($blog->poster);
                }

                $date = date('Y',time()).'/'.date('m',time());
                $path = $request->poster->store("uploads/blogs/{$date}/");
                $blog->poster = $path;
            }
            $blog->user_id = auth()->user()->id;
            $blog->cat_id = $request->cat_id;
            $blog->full_text = $request->full_text;
            if($request->published){
                $blog->published = 1;
            }else{
                $blog->published = 0;
            }
            $blog->save();

            $request->session()->flash('info',trans('admin.successfull_edited'));
        }else {
            $request->session()->flash('info',trans('admin.not_found_post'));
        }

        return redirect()->route('d-blogs');

    }
}
