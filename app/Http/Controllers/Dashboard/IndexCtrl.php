<?php

namespace App\Http\Controllers\Dashboard;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\User;
use App\Models\Blog;
use App\Models\Question;
use App\Models\Biling;

class IndexCtrl extends Controller
{
    public function index()
    {
        $count = (object)[
            'users' => User::count(),
            'blogs' => Blog::count(),
            'questions' => Question::count(),
            'billing' => Biling::count()
        ];
        return view('dashboard.index',compact('count'));
    }
}
