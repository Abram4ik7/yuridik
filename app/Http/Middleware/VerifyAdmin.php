<?php

namespace App\Http\Middleware;

use Closure;
use App\User;
use Illuminate\Support\Facades\Auth;

class VerifyAdmin
{
    public function handle($request, Closure $next, $guard = null)
    {
        if (! Auth::guard($guard)->user()->can('admin', User::class)) {
            return redirect('/');
        }
        return $next($request);
    }
}
