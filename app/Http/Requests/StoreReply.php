<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class StoreReply extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'files' => 'file|mimes:jpeg,bmp,png,pdf,gif,doc,docs,rtf,odt,txt,xls,xlsx|size:20480',
            'full_text' => 'required',
        ];
    }
}
