<?php

namespace App\Http\Requests\Auth;

use App\Rules\Auth\PhoneRule;
use App\Http\Requests\Request;

class LoginRequest extends Request
{
    public function rules()
    {
        return [
            'phone' => ['required', new PhoneRule],
        ];
    }

    public function phone(): int
    {
        return (int) 998 . $this->get('phone');
    }
}
