<?php

namespace App\Http\Requests\Auth;

use App\Role;
use App\City;

use App\Rules\Auth\RoleRule;
use App\Rules\Auth\PhoneRule;

use App\Http\Requests\Request;

class RegisterRequest extends Request
{
    public function rules()
    {
        return [
            'first_name' => ['required', 'string', 'max:255'],
            'last_name' => ['required', 'string', 'max:255'],
            'phone' => ['required', new PhoneRule],
            'email' => ['nullable', 'string', 'email', 'max:255'],
            'lang' => ['required', 'array'],
            'role' => ['required', new RoleRule],
            'city' => ['required', 'string', 'exists:cities,slug'],
        ];
    }

    public function firstName(): string
    {
        return (string) $this->get('first_name');
    }

    public function lastName(): string
    {
        return (string) $this->get('last_name');
    }

    public function phone(): int
    {
        return (int) 998 . $this->get('phone');
    }

    public function email(): string
    {
        return (string) $this->get('email');
    }

    public function lang(): array
    {
        return (array) $this->get('lang');
    }

    public function role(): Role
    {
        return Role::findBySlug($this->get('role'));
    }

    public function city(): City
    {
        return City::findBySlug($this->get('city'));
    }
}
