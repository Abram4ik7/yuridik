<?php

namespace App\Http\Requests\Admin;

use App\Role;
use App\City;

use App\Rules\Auth\RoleRule;
use App\Http\Requests\Request;

class UserUpdateRequest extends Request
{
    public function rules()
    {
        return [
            'first_name' => ['required', 'string', 'max:255'],
            'last_name' => ['required', 'string', 'max:255'],
            'email' => ['nullable', 'string', 'email', 'max:255'],
            'photo' => ['nullable', 'image'],
            'role' => ['required', new RoleRule],
            'city' => ['required', 'string', 'exists:cities,slug'],
        ];
    }

    public function firstName(): string
    {
        return (string) $this->get('first_name');
    }

    public function lastName(): string
    {
        return (string) $this->get('last_name');
    }

    public function email(): string
    {
        return (string) $this->get('email');
    }

    public function photo(): string
    {
        $path = null;

        if ($this->hasFile('photo')) {
            $path = $this->photo->store('uploads/users/' . date('Y/m'));
        }

        return (string) $path;
    }

    public function role(): Role
    {
        return Role::findBySlug($this->get('role'));
    }

    public function city(): City
    {
        return City::findBySlug($this->get('city'));
    }
}
