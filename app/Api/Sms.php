<?php

namespace App\Api;

use Carbon\Carbon;
use GuzzleHttp\Client;
use GuzzleHttp\Exception\ClientException;

class Sms
{
    protected $client;

    const URL = 'http://91.204.239.42:8083';
    const USERNAME = 'pputytx';
    const PASSWORD = 'pp0t@xx!';
    const ORIGINATOR = '3700';

    public function send(int $phone, string $text): bool
    {
        try {
            $this->getClient()->post('broker-api/send', [
                'json' => [
                    'messages' => [
                        'recipient' => $phone,
                        'message-id' => Carbon::now(),
                        'sms' => [
                            'originator' => self::ORIGINATOR,
                            'content' => [
                                'text' => 'Yuridik.uz - ' . $text,
                            ],
                        ],
                    ],
                ],
            ]);
        } catch (ClientException $exception) {
            return false;
        }

        return true;
    }

    protected function getClient(): Client
    {
        if (is_null($this->client)) {
            $this->client = new Client([
                'base_uri' => self::URL,
                'auth' => [
                    self::USERNAME,
                    self::PASSWORD,
                ],
            ]);
        }

        return $this->client;
    }
}
