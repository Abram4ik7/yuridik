<?php

namespace App;

use Illuminate\Support\Facades\App;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\HasMany;

class Role extends Model
{
    protected $fillable = [
        'slug', 'name', 'description', 'permissions',
    ];

    protected $casts = [
        'slug' => 'string',
        'name' => 'array',
        'description' => 'array',
        'permissions' => 'array',
    ];

    const ADMIN = 1;
    const MODER = 2;
    const ADVOCACY = 3;
    const LAWYER = 4;
    const LEGAL_COUNSEL = 5;
    const LEGAL_PERSON = 6;
    const CLIENT = 7;

    public function id(): int
    {
        return (int) $this->id;
    }

    public function slug(): string
    {
        return (string) $this->slug;
    }

    public function name(): string
    {
        return App::isLocale('uz') ? $this->name['uz'] : $this->name['ru'];
    }

    public function description(): string
    {
        return App::isLocale('uz') ? $this->description['uz'] : $this->description['ru'];
    }

    public function hasPermission(string $permission): bool
    {
        return $this->permissions[$permission] ?? false;
    }

    public function users(): HasMany
    {
        return $this->hasMany(User::class, 'role_id');
    }

    public static function findBySlug(string $slug): self
    {
        return static::where('slug', $slug)->firstOrFail();
    }
}
