<?php

namespace App\Policies;

use App\User;

class UserPolicy
{
    public function admin(User $user): bool
    {
        return $user->isAdmin() || $user->isModer();
    }

    public function update(User $user, User $subject): bool
    {
        return $user->isAdmin() && ! $subject->isAdmin();
    }

    public function delete(User $user, User $subject): bool
    {
        return $user->isAdmin() && ! $subject->isAdmin();
    }
}
