<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\BelongsTo;
use App\User;
use App\Models\File;

class Answer extends Model
{
    protected $fillable = [
        'user_id', 'full_text','question_id'
    ];

    protected $casts = [
        'user_id' => 'integer',
        'full_text' => 'text',
        'question_id' => 'integer',
    ];

    public function users(): BelongsTo
    {
        return $this->belongsTo(User::class, 'user_id');
    }

    public function files()
    {
        return $this->hasMany(File::class, 'doc_id','id')->where('type','answer');
    }


    public function sub_answers()
    {
        return $this->hasMany(self::class,'parrent_id','id');
    }

    public function ss_answers()
    {
        return $this->hasMany(self::class,'parrent_id','id');
    }
}
