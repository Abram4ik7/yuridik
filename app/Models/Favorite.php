<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Favorite extends Model
{
    protected $fillable = [
        'user_id','lawyer_id'
    ];

    protected $casts = [
        'user_id' => 'integer',
        'lawyer_id' => 'integer',
    ];
}
