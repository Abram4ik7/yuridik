<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use App\User;
use Illuminate\Database\Eloquent\Relations\BelongsTo;
use App\Models\Speciality;

class Blog extends Model
{
    protected $fillable = [
        'title', 'full_text','user_id','views','poster','likes','dislikes','cat_id','slug','published', 'lang'
    ];

    protected $casts = [
        'title' => 'string',
        'full_text' => 'text',
        'user_id' => 'integer',
        'views' => 'views',
        'poster' => 'string',
        'likes' => 'integer',
        'dislikes' => 'integer',
        'cat_id' => 'integer',
        'pulished' => 'integer',
        'lang' => 'string',
        'slug' => 'string',
    ];


    public function user(): BelongsTo
    {
        return $this->belongsTo(User::class,'id');
    }

    public function speciality(): BelongsTo
    {
        return $this->belongsTo(Speciality::class,'speciality_id');
    }


}
