<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class File extends Model
{
    public function files()
    {
        return $this->morphTo();
    }

    public function substr($maxLen = 20)
    {
        if (strlen($this->name) > $maxLen) {
            $characters = floor($maxLen / 2);
            return substr($this->name, 0, $characters) . '...' . substr($this->name, -1 * $characters);
        }
    }


}
