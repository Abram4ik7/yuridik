<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use App;

class Setting extends Model
{
    protected $fillable = [
        'title', 'keywords','description','phone','email','address','facebook_link','instagram_link','telegram_link','commission'
    ];

    protected $casts = [
        'title' => 'array',
        'keywords' => 'array',
        'description' => 'array',
        'phone' => 'string',
        'email' => 'string',
        'address' => 'array',
        'facebook_link' => 'string',
        'instagram_link' => 'string',
        'telegram_link' => 'string',
        'commission' => 'integer',
    ];


    public function name(): string
    {
        return App::isLocale('uz') ? $this->title['uz'] : $this->title['ru'];
    }

    public function name_ru() :string
    {
        return $this->title['ru'];
    }

    public function name_uz() :string
    {
        return $this->title['uz'];
    }

    public function keywords_uz(): string
    {
        return $this->keywords['uz'] ? $this->keywords['uz'] : '' ;
    }

    public function keywords_ru(): string
    {
        return $this->keywords['ru'] ? $this->keywords['ru'] : '' ;
    }

    public function description_ru(): string
    {
        return $this->description['ru'] ? $this->description['ru'] : '' ;
    }

    public function description_uz(): string
    {
        return $this->description['uz'] ? $this->description['uz'] : '' ;
    }

    public function address_uz(): string
    {
        return $this->address['uz'] ? $this->address['uz'] : '' ;
    }

    public function address_ru(): string
    {
        return $this->address['ru'] ? $this->address['ru'] : '' ;
    }

}
