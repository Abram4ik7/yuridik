<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use App;

class Speciality extends Model
{
    protected $fillable = [
        'name'
    ];

    protected $casts = [
        'name' => 'array',
    ];

    public function name(): string
    {
        return App::isLocale('uz') ? $this->name['uz'] : $this->name['ru'];
    }
}
