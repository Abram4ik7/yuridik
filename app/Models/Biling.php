<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Biling extends Model
{
    protected $fillable = [
        'order_id', 'amount','user_id','system','pay_type','status_pay'
    ];

    protected $casts = [
        'user_id' => 'integer',
        'order_id' => 'integer',
        'amount' => 'integer',
        'system' => 'string',
        'pay_type' => 'integer',
        'status_pay' => 'integer',
    ];
}
