<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\BelongsTo;
use App\User;
use App\Models\Answer;
use App\Models\Speciality;
use App\Models\File;

use App;

class Question extends Model
{
    protected $fillable = [
        'title', 'full_text','user_id','view','published','status_pay','region_id',
        'type_user', 'file', 'service_id', 'closed', 'price', 'lang', 'slug',
        'top_question', 'private_question'
    ];

    protected $casts = [
        'user_id' => 'integer',
        'order_id' => 'integer',
        'amount' => 'integer',
        'system' => 'string',
        'pay_type' => 'integer',
        'status_pay' => 'integer',
        'top_question' => 'integer',
        'private_question' => 'integer',
        'created_at' => 'datetime:d.m.Y, H:i',
        'updated_at' => 'datetime:d.m.Y, H:i',
    ];

    public function users() : BelongsTo
    {
        return $this->belongsTo(User::class, 'user_id');
    }

    public function answersCount(): int
    {
        return Answer::where('question_id',$this->id)->whereNull('parrent_id')->count();
    }

    public function speciality(): BelongsTo
    {
        return $this->belongsTo(Speciality::class,'speciality_id');
    }

    public function files()
    {
        return $this->hasMany(File::class, 'doc_id','id')->where('type','question');
    }

    public function additionals()
    {
        return $this->hasMany(self::class,'parrent_id','id');
    }

    public function additional_files()
    {
        return $this->hasMany(File::class, 'doc_id','id')->where('type','question_additional');
    }



}
