<?php

namespace App\Queries;
use App\Models\Answer;
use App\Models\Question;
use App;
use App\Models\File;
use Illuminate\Pagination\Paginator;


class Questions
{
    static function answers_sql($id, $service_id, $user_id)
    {
        $answers = Answer::where('question_id',$id)->whereNull('parrent_id');
        if(in_array($service_id, [ 1,2 ])) {
            if (auth()->check() && auth()->user()->isReply() || auth()->check() && $user_id == auth()->user()->id) {
                $data = $answers->get();
            } else {
                $data =  $answers->limit(1)->get();
            }
        } else {
            $data = $answers->get();
        }

        return $data;
    }


    static function listAllSql()
    {
        $sql = Question::where('lang', App::getLocale())
            ->where('published',1)
            ->orderBy("top_question",'desc')
            ->orderBy('id','desc')
            ->whereNull('parrent_id');

        if(auth()->check() && auth()->user()->isViewQuestions()) {
            $data = $sql->paginate(10);
        }else {
            $sql->where('private_question',0);
            $data = $sql->paginate(10);
        }

        return $data;
    }


    static function listPaidSql()
    {
        $sql = Question::where('lang', App::getLocale())
            ->where('status_pay',1)
            ->where('published',1)
            ->orderBy("top_question",'desc')
            ->orderBy('id','desc')
            ->whereNull('parrent_id');

        if(auth()->check() && auth()->user()->isViewQuestions()) {
            $data = $sql->paginate(10);
        }else {
            $sql->where('private_question',0);
            $data = $sql->paginate(10);
        }

        return $data;
    }

    static function listFreeSql()
    {
        $sql = Question::where('lang', App::getLocale())
            ->where('status_pay',3)
            ->where('published',1)
            ->orderBy("top_question",'desc')
            ->orderBy('id','desc')
            ->where('private_question',0)
            ->whereNull('parrent_id')
            ->paginate(10);


            return $sql;
    }


    static function CreateAdditionals($id, $request, $question)
    {
        $q = new Question;
        $q->user_id = auth()->user()->id;
        $q->title = 'Additional';
        $q->full_text = $request->full_text;
        $q->region_id = $question->region_id;
        $q->parrent_id = $id;
        $q->published = 1;
        $q->slug = "{$id}-additional";
        $q->save();

        if($request->file('files')){
            foreach($request->file('files') as $file) {
                $date = date('Y',time()).'/'.date('m',time());
                $path = $file->store("uploads/questions/{$date}");

                $filen = new File;
                $filen->path = $path;
                $filen->type = 'question_additional';
                $filen->doc_id = $q->id;
                $filen->name = $file->getClientOriginalName();
                $filen->filesize = $file->getSize();
                $filen->save();
            }
        }

        return $q;

    }
}
