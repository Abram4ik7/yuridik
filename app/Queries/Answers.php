<?php

namespace App\Queries;

use App\Models\Answer;
use App\Models\File;

class Answers
{

    static function ReplyCreate($id, $text)
    {
        $answer = new Answer;
        $answer->user_id = auth()->user()->id;
        $answer->full_text = $text;
        $answer->question_id = $id;
        $answer->save();


        //return $answer;
    }

    static function CreateInQuestion($id, $request)
    {
        $answer = new Answer;
        $answer->user_id = auth()->user()->id;
        $answer->full_text = $request->full_text;
        $answer->question_id = $id;
        $answer->save();

        if($request->file('files')){
            foreach($request->file('files') as $file) {

                $date = date('Y',time()).'/'.date('m',time());
                $path = $file->store("uploads/questions/{$date}");

                $filen = new File;
                $filen->path = $path;
                $filen->type = 'answer';
                $filen->doc_id = $answer->id;
                $filen->name = $file->getClientOriginalName();
                $filen->filesize = $file->getSize();
                $filen->save();
            }
        }

        return $answer;
    }


    static function CreateAdditionalAnswers($id, $request)
    {
        $answer = Answer::find($id);

        $additional = new Answer;
        $additional->question_id = $answer->question_id;
        $additional->parrent_id = $id;
        $additional->full_text = $request->text;
        $additional->user_id = auth()->user()->id;
        $additional->save();

        return $additional;
    }

}
