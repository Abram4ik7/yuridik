<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
*/

Route::group(['prefix' => '/', 'namespace' => 'Site'],function(){
    Route::get('/','IndexCtrl@index')->name('home');

    Route::prefix('lawyers')->group(function(){
       Route::get('/','LawyersCtrl@index')->name('lawyers');

       Route::post('/gratitude/{id}','LawyersCtrl@thanks');
    });

    Route::prefix('posts')->group(function(){
        Route::get('/','PostsCtrl@index')->name('posts');
        Route::get('/view/{id}/{slug}','PostsCtrl@view')->name('posts.view');
    });

    Route::prefix('questions')->group(function(){
        Route::get('/','QuestionsCtrl@list')->name('questions');
        Route::get('/paid','QuestionsCtrl@paid')->name('questions.paid');
        Route::get('/free','QuestionsCtrl@free')->name('questions.free');
        Route::get('view/{id}/{slug}', 'QuestionsCtrl@view')->name('questions.view');

        Route::post('reply/ajax', 'ReplyCtrl@reply_ajax')->name('questions.ajax_reply');
        Route::post('reply/send/{id}', 'ReplyCtrl@reply')->name('questions.answer-in');
        Route::post('additional/{id}', 'ReplyCtrl@additional')->name('questions.additional');
        Route::post('additional/answer/{id}', 'ReplyCtrl@additional_answer')->name('questions.additional.answer');
    });

    Route::prefix('users')->group(function(){
        Route::get('/profile/{id}/{slug}','UsersCtrl@profile')->name('users-view');
    });


    Route::prefix('download')->group(function(){
        Route::get('/file/{id}','DownloadCtrl@download')->name('download_file');
    });


    Route::prefix('question')->group(function(){
        Route::get('/create','CreateQuestionCtrl@create')->name('question.create');
    });


});

Route::group(['prefix' => 'auth'], function () {
    Route::get('/', 'AuthController@index')->name('login');

    Route::post('login', 'AuthController@login');
    Route::post('register', 'AuthController@register');
    Route::post('verify/{phone}/code/{code}', 'AuthController@verify');
    Route::post('send/{phone}', 'AuthController@send');
    Route::post('logout', 'AuthController@logout');
});


Route::group(['prefix' => 'dashboard', 'namespace' => 'Dashboard'],function(){
    Route::get('/','IndexCtrl@index')->name('dashboard');
    Route::get('/questions','IndexCtrl@index')->name('d-questions');
    Route::get('/billing','IndexCtrl@index')->name('d-billing');

    Route::prefix('blogs')->group(function(){
        Route::get('/','BlogsCtrl@index')->name('d-blogs');
        Route::get('/delete/{id}','BlogsCtrl@delete')->name('d-blogs-delete');
        Route::get('/add','BlogsCtrl@add')->name('d-blogs-add');
        Route::post('/create','BlogsCtrl@create')->name('d-blogs-create');
        Route::get('/edit/{id}','BlogsCtrl@edit')->name('d-blogs-edit');
        Route::post('/update/{id}','BlogsCtrl@update')->name('d-blogs-update');
    });

    Route::prefix('users')->group(function(){
        Route::get('/','UsersCtrl@index')->name('d-users');
        Route::get('/search','UsersCtrl@search')->name('d-users-search');
        Route::get('/delete/{id}','UsersCtrl@delete')->name('d-users-delete');
        Route::get('/edit/{id}','UsersCtrl@edit')->name('d-users-edit');
        Route::post('/update/{id}','UsersCtrl@update')->name('d-users-update');
    });

    Route::prefix('settings')->group(function(){
        Route::get('/','SettingsCtrl@index')->name('d-settings');
        Route::post('/update','SettingsCtrl@update')->name('d-settings-update');
    });



});


Route::get('test',function(){
    $user = \App\User::find(2);
    Auth::login($user,true);
});