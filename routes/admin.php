<?php

/*
|--------------------------------------------------------------------------
| Admin Routes
|--------------------------------------------------------------------------
*/

Route::group(['namespace' => 'Admin'], function () {
    Route::get('/', 'DashboardController@dashboard')->name('admin.dashboard');

    Route::group(['prefix' => 'users'], function () {
        Route::get('/', 'UsersController@index')->name('admin.users');

        Route::post('update/{phone}', 'UsersController@update');
        Route::post('delete/{phone}', 'UsersController@delete');
    });
});
