<?php

Route::bind('phone', function (int $phone) {
    return App\User::findByPhone($phone);
});
