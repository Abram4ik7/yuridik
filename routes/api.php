<?php

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
*/

Route::group(['prefix' => 'users'], function () {
    Route::get('/', 'ApiController@users');
});

Route::namespace('Site')->prefix('question')->group(function(){
    Route::get('cities','ApiController@cities');
    Route::get('specialites','ApiController@speciality');
    Route::get('checkAuth','ApiController@checkAuth');
    Route::post('sendSms','ApiController@sendSms');
    Route::post('send/verify','ApiController@verify');
    Route::get('services','ApiController@getServices');
});