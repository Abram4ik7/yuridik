<?php

use App\City;
use Illuminate\Support\Str;
use Illuminate\Database\Seeder;

class CitiesSeeder extends Seeder
{
    public function run()
    {
        City::create([
            'slug' => Str::slug('No region'),

            'name' => [
                'uz' => 'Region yo\'q',
                'ru' => 'Нет региона',
            ],
        ]);

        City::create([
            'slug' => Str::slug('Tashkent city'),

            'name' => [
                'uz' => 'Toshkent shahri',
                'ru' => 'Ташкент',
            ],
        ]);

        City::create([
            'slug' => Str::slug('Tashkent region'),

            'name' => [
                'uz' => 'Toshkent viloyati',
                'ru' => 'Ташкентская область',
            ],
        ]);

        City::create([
            'slug' => Str::slug('Andijan'),

            'name' => [
                'uz' => 'Andijon',
                'ru' => 'Андижан',
            ],
        ]);

        City::create([
            'slug' => Str::slug('Bukhara'),

            'name' => [
                'uz' => 'Buxoro',
                'ru' => 'Бухара',
            ],
        ]);

        City::create([
            'slug' => Str::slug('Jizzakh'),

            'name' => [
                'uz' => 'Jizzax',
                'ru' => 'Джизак',
            ],
        ]);

        City::create([
            'slug' => Str::slug('Karakalpakstan'),

            'name' => [
                'uz' => 'Qoraqalpog\'iston',
                'ru' => 'Каракалпакстан',
            ],
        ]);

        City::create([
            'slug' => Str::slug('Kashkadarya'),

            'name' => [
                'uz' => 'Qashqadaryo',
                'ru' => 'Кашкадарья',
            ],
        ]);

        City::create([
            'slug' => Str::slug('Navoi'),

            'name' => [
                'uz' => 'Navoiy',
                'ru' => 'Навои',
            ],
        ]);

        City::create([
            'slug' => Str::slug('Namangan'),

            'name' => [
                'uz' => 'Namangan',
                'ru' => 'Наманган',
            ],
        ]);

        City::create([
            'slug' => Str::slug('Samarkand'),

            'name' => [
                'uz' => 'Samarqand',
                'ru' => 'Самарканд',
            ],
        ]);

        City::create([
            'slug' => Str::slug('Surkhandarya'),

            'name' => [
                'uz' => 'Surxondaryo',
                'ru' => 'Сурхандарья',
            ],
        ]);

        City::create([
            'slug' => Str::slug('Syrdarya'),

            'name' => [
                'uz' => 'Sirdaryo',
                'ru' => 'Сырдарья',
            ],
        ]);

        City::create([
            'slug' => Str::slug('Fergana'),

            'name' => [
                'uz' => 'Farg\'ona',
                'ru' => 'Фергана',
            ],
        ]);

        City::create([
            'slug' => Str::slug('Khorezm'),

            'name' => [
                'uz' => 'Xorazm',
                'ru' => 'Хорезм',
            ],
        ]);
    }
}
