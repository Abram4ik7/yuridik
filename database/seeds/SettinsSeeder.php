<?php

use Illuminate\Database\Seeder;
use App\Models\Setting;

class SettinsSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        Setting::create([
            'title' => [
                'uz' => 'Yuridik.uz',
                'ru' => 'Yuridik.ru'
            ]

        ]);
    }
}
