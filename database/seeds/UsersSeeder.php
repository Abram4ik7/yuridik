<?php

use App\Role;
use App\City;
use App\User;

use Illuminate\Database\Seeder;

class UsersSeeder extends Seeder
{
    public function run()
    {
        User::create([
            'first_name' => 'Sukhrob',
            'last_name' => 'Karshiev',
            'phone' => 998913388597,
            'email' => 'iaxel0297@gmail.com',
            'lang' => ['ru'],
            'role_id' => Role::ADMIN,
            'city_id' => City::TASHKENT_CITY,
            'verify' => true,
            'verify_code' => null,
        ]);

        factory(User::class, 99)->create();
    }
}
