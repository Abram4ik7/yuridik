<?php

use Illuminate\Database\Seeder;
use Faker\Factory as Faker;


class QuestionFakerSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $faker = Faker::create();
        foreach (range(1,20) as $index) {
            DB::table('questions')->insert([
                'title' => $faker->realText($maxNbChars = 60, $indexSize = 1),
                'full_text' => $faker->paragraph($nbSentences = 13, $variableNbSentences = true),
                'user_id' => 1,
                'region_id' => 1,
                'published' => 1,
                'status_pay' => 3,
                'slug' => str_slug($faker->realText($maxNbChars = 60, $indexSize = 1)),
                'lang' => 'uz',
            ]);
        }

        foreach (range(1,20) as $index) {
            DB::table('questions')->insert([
                'title' => $faker->realText($maxNbChars = 60, $indexSize = 1),
                'full_text' => $faker->paragraph($nbSentences = 13, $variableNbSentences = true),
                'user_id' => 1,
                'region_id' => 1,
                'published' => 1,
                'status_pay' => 3,
                'slug' => str_slug($faker->realText($maxNbChars = 60, $indexSize = 1)),
                'lang' => 'ru',
            ]);
        }

        foreach (range(1,20) as $index) {
            DB::table('questions')->insert([
                'title' => $faker->realText($maxNbChars = 60, $indexSize = 1),
                'full_text' => $faker->paragraph($nbSentences = 13, $variableNbSentences = true),
                'user_id' => 1,
                'region_id' => 1,
                'published' => 1,
                'status_pay' => 1,
                'price' => 25000,
                'service_id' => 1,
                'slug' => str_slug($faker->realText($maxNbChars = 60, $indexSize = 1)),
                'lang' => 'uz',
            ]);
        }

    }
}
