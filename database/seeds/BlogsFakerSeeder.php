<?php

use Illuminate\Database\Seeder;
use Faker\Factory as Faker;

class BlogsFakerSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $faker = Faker::create();
        foreach (range(1,20) as $index) {
            DB::table('blogs')->insert([
                'title' => $faker->realText($maxNbChars = 60, $indexSize = 1),
                'full_text' => $faker->paragraph,
                'user_id' => 1,
                'speciality_id' => 1,
                'published' => 1,
                'slug' => str_slug($faker->realText($maxNbChars = 60, $indexSize = 1)),
                'lang' => 'uz',
            ]);
        }

        foreach (range(1,20) as $index) {
            DB::table('blogs')->insert([
                'title' => $faker->realText($maxNbChars = 60, $indexSize = 1),
                'full_text' => $faker->text,
                'user_id' => 1,
                'speciality_id' => 1,
                'published' => 1,
                'slug' => str_slug($faker->realText($maxNbChars = 60, $indexSize = 1)),
                'lang' => 'ru',
            ]);
        }
    }
}
