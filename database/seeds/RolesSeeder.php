<?php

use App\Role;
use Illuminate\Support\Str;
use Illuminate\Database\Seeder;

class RolesSeeder extends Seeder
{
    public function run()
    {
        Role::create([
            'slug' => Str::slug('Admin'),

            'name' => [
                'uz' => 'Administrator',
                'ru' => 'Администратор',
            ],

            'description' => [
                'uz' => 'Administratorga qisqacha ta\'rif.',
                'ru' => 'Краткое описание администратора.',
            ],

            'permissions' => [
                //
            ],
        ]);

        Role::create([
            'slug' => Str::slug('Moder'),

            'name' => [
                'uz' => 'Moderator',
                'ru' => 'Модератор',
            ],

            'description' => [
                'uz' => 'Moderatorga qisqacha ta\'rif.',
                'ru' => 'Краткое описание модератора.',
            ],

            'permissions' => [
                //
            ],
        ]);

        Role::create([
            'slug' => Str::slug('Advocacy'),

            'name' => [
                'uz' => 'Advokatura',
                'ru' => 'Адвокатура',
            ],

            'description' => [
                'uz' => 'Advokaturaga qisqacha ta\'rif.',
                'ru' => 'Краткое описание адвокатуры.',
            ],

            'permissions' => [
                //
            ],
        ]);

        Role::create([
            'slug' => Str::slug('Lawyer'),

            'name' => [
                'uz' => 'Advokat',
                'ru' => 'Адвокат',
            ],

            'description' => [
                'uz' => 'Advokatga qisqacha ta\'rif.',
                'ru' => 'Краткое описание адвоката.',
            ],

            'permissions' => [
                //
            ],
        ]);

        Role::create([
            'slug' => Str::slug('Legal Counsel'),

            'name' => [
                'uz' => 'Huquqshunos',
                'ru' => 'Юрисконсульт',
            ],

            'description' => [
                'uz' => 'Huquqshunosga qisqacha ta\'rif.',
                'ru' => 'Краткое описание юрисконсульта.',
            ],

            'permissions' => [
                //
            ],
        ]);

        Role::create([
            'slug' => Str::slug('Legal person'),

            'name' => [
                'uz' => 'Yuridik shaxs',
                'ru' => 'Юридическое лицо',
            ],

            'description' => [
                'uz' => 'Yuridik shaxsga qisqacha ta\'rif.',
                'ru' => 'Краткое описание юридического лица.',
            ],

            'permissions' => [
                //
            ],
        ]);

        Role::create([
            'slug' => Str::slug('Client'),

            'name' => [
                'uz' => 'Mijoz',
                'ru' => 'Клиент',
            ],

            'description' => [
                'uz' => 'Mijozga qisqacha ta\'rif.',
                'ru' => 'Краткое описание клиента.',
            ],

            'permissions' => [
                //
            ],
        ]);
    }
}
