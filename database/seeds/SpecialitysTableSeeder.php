<?php

use Illuminate\Database\Seeder;
use App\Models\Speciality;

class SpecialitysTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $s = new Speciality;
        $s->name = ['uz' => 'Inson huquqlari', 'ru' => 'Гражданское право'];
        $s->save();
    }
}
