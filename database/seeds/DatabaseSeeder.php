<?php

use Illuminate\Database\Seeder;

class DatabaseSeeder extends Seeder
{
    public function run()
    {
        $this->call(RolesSeeder::class);
        $this->call(CitiesSeeder::class);
        $this->call(UsersSeeder::class);
        $this->call(SettinsSeeder::class);
        $this->call(QuestionFakerSeeder::class);
        $this->call(BlogsFakerSeeder::class);
        $this->call(SpecialitysTableSeeder::class);
    }
}
