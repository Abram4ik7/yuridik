<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateFilesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('files', function (Blueprint $table) {
            $table->increments('id');
            $table->string('name');
            $table->string('path');
            $table->integer('doc_id');//question yoki answerni IDSi
            $table->string('type');// question or answer boladi
            $table->integer('filesize')->default(0);//
            $table->timestamps();
        });
    }


    public function down()
    {
        Schema::dropIfExists('files');
    }

}
