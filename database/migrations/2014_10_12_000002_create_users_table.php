<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateUsersTable extends Migration
{
    public function up()
    {
        Schema::create('users', function (Blueprint $table) {
            $table->increments('id');

            $table->string('first_name')->nullable();
            $table->string('last_name')->nullable();

            $table->string('phone', 12)->unique();
            $table->string('email')->nullable();
            $table->string('photo')->nullable();

            $table->jsonb('lang')->nullable();

            $table->unsignedInteger('role_id')->nullable();
            $table->unsignedInteger('city_id')->nullable();

            $table->boolean('verify')->default(false);
            $table->string('verify_code', 5)->nullable();
            $table->integer('thanks')->default(0);

            $table->ipAddress('ip')->default('127.0.0.1');

            $table->rememberToken();
            $table->timestamps();

            $table->foreign('role_id')->references('id')->on('roles');
            $table->foreign('city_id')->references('id')->on('cities');
        });
    }

    public function down()
    {
        Schema::dropIfExists('users');
    }
}
