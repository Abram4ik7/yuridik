<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateBilingsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('bilings', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('order_id');
            $table->integer('amount');
            $table->string('system');
            $table->integer('user_id');
            $table->integer('pay_type');
            $table->integer('status_pay')->default(0);
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('bilings');
    }
}
