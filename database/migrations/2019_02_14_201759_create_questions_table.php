<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateQuestionsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('questions', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('user_id');
            $table->string('title',500);
            $table->text('full_text');
            $table->integer('region_id');
            $table->integer('parrent_id')->nullable();
            $table->integer('speciality_id')->default(1);
            $table->integer('views')->default(0);
            $table->integer('type_user')->default(1);
            //$table->string('file',500)->nullable();
            $table->integer('service_id')->default(3);//3 bu bepul savol boladi
            $table->integer('published')->default(0);
            $table->integer('status_pay')->default(0);
            $table->integer('closed')->default(0);//closed 0 yopilmagan 1 yopilgan
            $table->integer('price')->nullable();//
            $table->integer('private_question')->default(0);//
            $table->integer('top_question')->default(0);//
            $table->string('lang',3)->default('uz');
            $table->string('slug',500);
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('questions');
    }
}
