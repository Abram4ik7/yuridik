<?php

use App\Role;
use App\City;
use App\User;

use Illuminate\Support\Str;
use Faker\Generator as Faker;

/*
|--------------------------------------------------------------------------
| Model Factories
|--------------------------------------------------------------------------
*/

$factory->define(User::class, function (Faker $faker) {
    return [
        'first_name' => $faker->firstName,
        'last_name' => $faker->lastName,
        'phone' => 998 . rand(90, 99) . rand(1000000, 9999999),
        'email' => $faker->email,
        'photo' => null,
        'lang' => ['uz', 'ru'],
        'role_id' => rand(Role::ADVOCACY, Role::CLIENT),
        'city_id' => rand(City::TASHKENT_CITY, City::KHOREZM),
        'verify' => true,
        'verify_code' => null,
        'ip' => $faker->ipv4,
        'remember_token' => Str::random(10),
    ];
});
