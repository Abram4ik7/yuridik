let mix = require('laravel-mix');

/*
 |--------------------------------------------------------------------------
 | Mix Asset Management
 |--------------------------------------------------------------------------
 */

mix
    .js('resources/js/app.js', 'public/js')
    .scripts([
        //'public/assets/js/analytics.js',

        'public/assets/js/jquery.min.js',
        'public/assets/js/preloader.js',
        'public/assets/js/highlight.js',
        'public/assets/js/in-view.min.js',
        'public/assets/js/jquery.min.js',
        //'public/assets/js/js',
        'public/assets/js/swiper.min.js',
        'public/assets/js/theme.min.js',
        'public/assets/js/script.js',
        'public/assets/js/bootstrap.bundle.min.js',

    ], 'public/js/all.js')

    .scripts([
        'resources/js/others/custom-file-input.js'
    ], 'public/js/custom-file-input.js')


    .extract([
        'axios',
        'bootstrap',
        'jquery',
        'lodash',
        'popper.js',
        'select2',
        'vue',
        'vue-async-computed',
        'vue-i18n',
        'vue-toasted'
    ])

    .setPublicPath('public')
    .sass('resources/sass/app.scss', 'public/css')
    .sass('resources/sass/auth.scss', 'public/css')
    .sass('resources/sass/admin.scss', 'public/css')
    //.sass('resources/sass/dapp.scss', 'public/css')

    .styles([
        'public/assets/css/all.min.css',
        'public/assets/css/atom-one-dark.min.css',
        'public/assets/css/font-awesome.min.css',
        'public/assets/css/fonts.css',
        'public/assets/css/media.css',
        'public/assets/css/styles.css',
        'public/assets/css/swiper.min.css',
        'public/assets/css/theme.min.css',
        'public/assets/css/translateelement.css',
    ], 'public/assets/css/all.css')

    .styles([
        'public/css/dashboard.css',
        'resources/sass/dapp.scss'
    ], 'public/css/dapp.css')

    .version();
