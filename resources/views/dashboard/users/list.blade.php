@extends('dashboard.layouts.app')
@section('title', trans('admin.users').' - ')

@section('content')
    <div class="d-flex justify-content-between flex-wrap flex-md-nowrap align-items-center pt-3 pb-2 mb-3 border-bottom">
        <h1 class="h2">@lang('admin.users')</h1>

        <div class="btn-toolbar mb-2 mb-md-0">
            <form action="{{ Route('d-users-search') }}" method="get">
            <div class="input-group mb-3">

                    <input type="text" class="form-control" name="q" placeholder="@lang('admin.id_or_phone')" aria-describedby="button-addon2">
                    <div class="input-group-append">
                        <button class="btn btn-outline-secondary" type="submit" id="button-addon2"><i class="fas fa-search"></i></button>
                    </div>



            </div>
            </form>


        </div>
    </div>

    @if(!empty(Session::get('info')))
        <div class="alert alert-info">
            <i class="fas fa-info-circle"></i> {{Session::get('info')}}
        </div>
    @endif

    <table class="table">
        <thead class="thead-dark">
        <tr>
            <th scope="col">ID</th>
            <th scope="col" class="text-center">@lang('admin.poster')</th>
            <th scope="col">@lang('admin.fio')</th>
            <th scope="col" class="text-center">@lang('admin.phone')</th>
            <th scope="col" class="text-center">@lang('admin.role')</th>
            <th scope="col">@lang('admin.register_date')</th>
            <th scope="col">@lang('admin.actions')</th>
        </tr>
        </thead>
        <tbody>
        @foreach($users as $user)
            <tr>
                <th scope="row">{{$user->id}}</th>
                <td class="text-center">
                    @if($user->photo and is_file($user->photo))
                        <img src="/{{$user->photo}}" width="50" class="rounded" alt="">
                    @else
                        <img src="/images/noavatar.png" width="50" class="rounded" alt="">
                    @endif
                </td>
                <td>
                    {{$user->first_name}} {{ $user->last_name }}
                </td>
                <td class="text-center">
                    +{{ $user->phone }}
                </td>
                <td class="text-center">
                    {{$user->role->name()}}
                </td>
                <td>{{date('d.m.Y, H:i',strtotime($user->created_at))}}</td>
                <td class="text-center">
                    <a href="{{Route('d-users-edit',[$user->id])}}" data-toggle="tooltip" data-placement="top" title="@lang('admin.edit')" class="btn btn-sm btn-primary"><i class="fas fa-edit"></i></a>
                    @if(auth()->user()->isAdmin())
                        <a href="{{Route('d-users-delete',[$user->id])}}" data-toggle="tooltip" data-placement="top" title="@lang('admin.delete')"  class="btn btn-sm btn-danger" onclick="return confirm('@lang('admin.perform_delete')')"><i class="fas fa-trash"></i></a>
                    @endif
                </td>
            </tr>
        @endforeach
        </tbody>
    </table>

    {{$users->links()}}

@endsection

@push('js')
    <script>
        $(function () {
            $('[data-toggle="tooltip"]').tooltip()
        })
    </script>
@endpush