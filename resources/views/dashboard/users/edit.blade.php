@extends('dashboard.layouts.app')
@section('title', trans('admin.edit').' - ')

@section('content')
    <div class="d-flex justify-content-between flex-wrap flex-md-nowrap align-items-center pt-3 pb-2 mb-3 border-bottom">
        <h1 class="h2">@lang('admin.edit')</h1>
    </div>

    <form action="{{ Route('d-users-update',[$data->id]) }}" method="post" enctype="multipart/form-data">
        <div class="form-group">
            <label for="firstname">@lang('admin.firstname')</label>
            <input type="text" class="form-control" required name="firstname" value="{{ $data->first_name }}" id="firstname" placeholder="@lang('admin.firstname')">
        </div>
        <div class="form-group">
            <label for="lastname">@lang('admin.lastname')</label>
            <input type="text" class="form-control"  required id="lastname"  name="lastname" value="{{ $data->last_name }}" placeholder="@lang('admin.lastname')">
        </div>

        <div class="form-group">
            <label for="phone">@lang('admin.phone')</label>
            <input type="text" class="form-control" required id="phone" name="phone" value="{{ $data->phone }}"  placeholder="@lang('admin.phone')">
        </div>

        <div class="form-group">
            <label for="file">@lang('admin.poster') @if(!is_file($data->photo)) <font color="red"><i class="fas fa-ban" data-toggle="tooltip" data-placement="top" title="@lang('admin.file_not_found')"></i></font> @else <font color="green"><i data-toggle="tooltip" data-placement="top" title="@lang('admin.file_empty')" class="fas fa-check"></i></font> @endif</label>
            <input type="file" name="photo" class="form-control" id="file"  placeholder="@lang('admin.poster')">
        </div>

        <div class="form-group">
            <label for="role_id">@lang('admin.role')</label>
            <select class="form-control" name="role_id" id="role_id">
                @foreach($roles as $role)
                    @if($role->id == 1)
                        @if(auth()->user()->isAdmin())
                            <option @if($data->role_id == $role->id) selected @endif value="{{ $role->id }}">{{$role->name()}}</option>
                        @endif
                    @else
                        <option @if($data->role_id == $role->id) selected @endif value="{{ $role->id }}">{{$role->name()}}</option>
                    @endif
                @endforeach
            </select>
        </div>


        <div class="form-group">
            <label for="city_id">@lang('admin.city')</label>
            <select class="form-control" name="city_id" id="city_id">
                @foreach($citys as $city)
                        <option @if($data->city_id == $city->id) selected @endif value="{{ $city->id }}">{{$city->name()}}</option>
                @endforeach
            </select>
        </div>

        <div class="form-group">
            <label for="lang">@lang('admin.lang')</label>
            <input type="text" class="form-control"  id="lang" disabled value="{{ implode(', ',$data->lang) }}" >
        </div>

        <div class="form-group">
            <label for="ip">IP</label>
            <input type="text" class="form-control" required id="ip" disabled value="{{ $data->ip }}" >
        </div>


        @csrf

        <button type="submit" class="btn btn-primary"><i class="fas fa-save"></i> @lang('admin.save')</button>
    </form>

@endsection

@push('js')
    <script>
        $(function () {
            $('[data-toggle="tooltip"]').tooltip()
        })
    </script>
@endpush