@extends('dashboard.layouts.app')

@section('content')

        <div class="d-flex justify-content-between flex-wrap flex-md-nowrap align-items-center pt-3 pb-2 mb-3 border-bottom">
            <h1 class="h2">@lang('admin.home')</h1>
        </div>


        <div class="col-md-12">
            <div class="row">
                <div class="col-md-3 card-info card-blue">
                    <div class="col-md-4 card-fa">
                        <i class="fas fa-users"></i>
                    </div>

                    <div class="col-md-8 card-count">
                        {{number_format($count->users,0,'',' ')}}
                    </div>

                    <div class="col-md-12 cart-title">
                        @lang('admin.users')
                    </div>

                    <div class="col-md-12 card-link">
                        <a href="{{route('d-users')}}"><i class="fas fa-eye"></i> @lang('admin.more')</a>
                    </div>
                </div>{{--item--}}


                <div class="col-md-3 card-info card-green">
                    <div class="col-md-4 card-fa">
                        <i class="fas fa-question-circle"></i>
                    </div>

                    <div class="col-md-8 card-count">
                        {{number_format($count->questions,0,'',' ')}}

                    </div>
                    <div class="col-md-12 cart-title">
                        @lang('admin.questions')
                    </div>
                    <div class="col-md-12 card-link">
                        <a href="{{route('d-questions')}}"><i class="fas fa-eye"></i> @lang('admin.more')</a>
                    </div>
                </div>{{--item--}}


                <div class="col-md-3 card-info card-blog">
                    <div class="col-md-4 card-fa">
                        <i class="fas fa-blog"></i>
                    </div>

                    <div class="col-md-8 card-count">
                        {{number_format($count->blogs,0,'',' ')}}
                    </div>
                    <div class="col-md-12 cart-title">
                        @lang('admin.blogs')
                    </div>
                    <div class="col-md-12 card-link">
                        <a href="{{route('d-blogs')}}"><i class="fas fa-eye"></i> @lang('admin.more')</a>
                    </div>
                </div>{{--item--}}


                <div class="col-md-3 card-info card-billing">
                    <div class="col-md-4 card-fa">
                        <i class="fas fa-credit-card"></i>
                    </div>

                    <div class="col-md-8 card-count">
                        {{number_format($count->billing,0,'',' ')}}
                    </div>
                    <div class="col-md-12 cart-title">
                        @lang('admin.billing')
                    </div>
                    <div class="col-md-12 card-link">
                        <a href="{{route('d-billing')}}"><i class="fas fa-eye"></i> @lang('admin.more')</a>
                    </div>
                </div>{{--item--}}


            </div>
        </div>
@endsection