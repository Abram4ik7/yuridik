@extends('dashboard.layouts.app')
@section('title', trans('admin.settings').' - ')

@section('content')
    <div class="d-flex justify-content-between flex-wrap flex-md-nowrap align-items-center pt-3 pb-2 mb-3 border-bottom">
        <h1 class="h2">@lang('admin.settings')</h1>
    </div>

    @if(!empty(Session::get('info')))
        <div class="alert alert-info">
            <i class="fas fa-info-circle"></i> {{Session::get('info')}}
        </div>
    @endif

    <ul class="nav nav-tabs" id="myTab" role="tablist">
        <li class="nav-item">
            <a class="nav-link active" id="home-tab" data-toggle="tab" href="#home" role="tab" aria-controls="home" aria-selected="true">UZ</a>
        </li>
        <li class="nav-item">
            <a class="nav-link" id="profile-tab" data-toggle="tab" href="#profile" role="tab" aria-controls="profile" aria-selected="false">RU</a>
        </li>

    </ul>


    <form action="{{ Route('d-settings-update') }}" method="post" enctype="multipart/form-data">


        <div class="tab-content" id="myTabContent">

            <div class="tab-pane fade show active" id="home" role="tabpanel" aria-labelledby="home-tab">
                <div class="form-group">
                    <label for="title_uz">@lang('admin.title') UZ</label>
                    <input type="text" class="form-control" required name="title_uz" value="{{ $data->name_uz() }}" id="title_uz" placeholder="@lang('admin.title')">
                </div>


                <div class="form-group">
                    <label for="keywords_uz">@lang('admin.keywords') UZ</label>
                    <input type="text" class="form-control"  name="keywords_uz" value="{{ $data->keywords_uz() }}" id="keywords_uz" placeholder="@lang('admin.keywords')">
                </div>

                <div class="form-group">
                    <label for="description_uz">@lang('admin.description') UZ</label>
                    <input type="text" class="form-control"  name="description_uz" value="{{ $data->description_uz() }}" id="description_uz" placeholder="@lang('admin.description')">
                </div>

                <div class="form-group">
                    <label for="address_uz">@lang('admin.address') UZ</label>
                    <input type="text" class="form-control"  name="address_uz" value="{{ $data->address_uz() }}" id="address_uz" placeholder="@lang('admin.address')">
                </div>
            </div>{{--uz--}}

            <div class="tab-pane fade" style="margin-top: 20px" id="profile" role="tabpanel" aria-labelledby="profile-tab">
                <div class="form-group">
                    <label for="title_ru">@lang('admin.title') RU</label>
                    <input type="text" class="form-control" required name="title_ru" value="{{ $data->name_ru() }}" id="title_ru" placeholder="@lang('admin.title')">
                </div>

                <div class="form-group">
                    <label for="keywords_ru">@lang('admin.keywords') RU</label>
                    <input type="text" class="form-control"  name="keywords_ru" value="{{ $data->keywords_ru() }}" id="keywords_ru" placeholder="@lang('admin.keywords')">
                </div>


                <div class="form-group">
                    <label for="description_ru">@lang('admin.description') RU</label>
                    <input type="text" class="form-control"  name="description_ru" value="{{ $data->description_ru() }}" id="description_ru" placeholder="@lang('admin.description')">
                </div>

                <div class="form-group">
                    <label for="address_ru">@lang('admin.address') Ru</label>
                    <input type="text" class="form-control"  name="address_ru" value="{{ $data->address_ru() }}" id="address_ru" placeholder="@lang('admin.address')">
                </div>

            </div>{{--ru--}}

        </div>




        <div class="form-group">
            <label for="phone">@lang('admin.phone')</label>
            <input type="text" class="form-control"  name="phone" value="{{ $data->phone }}" id="phone" placeholder="@lang('admin.phone')">
        </div>


        <div class="form-group">
            <label for="facebook_link">@lang('admin.facebook_link')</label>
            <input type="text" class="form-control"  name="facebook_link" value="{{ $data->facebook_link }}" id="facebook_link" placeholder="@lang('admin.facebook_link')">
        </div>

        <div class="form-group">
            <label for="instagram_link">@lang('admin.instagram_link')</label>
            <input type="text" class="form-control"  name="instagram_link" value="{{ $data->instagram_link }}" id="instagram_link" placeholder="@lang('admin.instagram_link')">
        </div>


        <div class="form-group">
            <label for="telegram_link">@lang('admin.telegram_link')</label>
            <input type="text" class="form-control"  name="telegram_link" value="{{ $data->telegram_link }}" id="telegram_link" placeholder="@lang('admin.telegram_link')">
        </div>


        <div class="form-group">
            <label for="phone">@lang('admin.phone')</label>
            <input type="text" class="form-control"  name="phone" value="{{ $data->phone }}" id="phone" placeholder="@lang('admin.phone')">
        </div>

        <div class="form-group">
            <label for="phone">@lang('admin.email')</label>
            <input type="email" class="form-control"  name="email" value="{{ $data->email }}" id="email" placeholder="@lang('admin.email')">
        </div>

        <div class="input-group mb-3">
            <div class="input-group-prepend">
                <span class="input-group-text" id="basic-addon1">%</span>
            </div>
            <input type="text" class="form-control" value="{{$data->commission}}" name="commission" placeholder="commission" aria-label="commission" aria-describedby="basic-addon1">
        </div>



        @csrf

        <button type="submit" class="btn btn-primary"><i class="fas fa-save"></i> @lang('admin.save')</button>
    </form>

@endsection

@push('js')
    <script>
        $(function () {
            $('[data-toggle="tooltip"]').tooltip()
        })
    </script>
@endpush