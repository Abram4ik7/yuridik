@extends('dashboard.layouts.app')
@section('title', trans('admin.edit').' - ')

@section('content')
    <style>
        .ck-editor__editable {
            min-height: 500px;
        }
    </style>
    <div class="d-flex justify-content-between flex-wrap flex-md-nowrap align-items-center pt-3 pb-2 mb-3 border-bottom">
        <h1 class="h2">@lang('admin.edit')</h1>
    </div>

    <form style="margin-bottom: 30px" action="{{Route('d-blogs-update',[$data->id])}}" method="post" enctype="multipart/form-data">
        <div class="form-group">
            <label for="title">@lang('admin.title')</label>
            <input type="text" class="form-control" id="title" name="title" value="{{$data->title}}"  placeholder="@lang('admin.title')">
        </div>

        <div class="form-group">
            <label for="cats">@lang('admin.cats')</label>
            <select name="cat_id" class="form-control" id="cats">
                <option value="1">1</option>
                <option>2</option>
                <option>5</option>
            </select>
        </div>

        <div class="form-group">
            <label for="lang">@lang('admin.lang')</label>
            <select class="form-control" name="lang" id="lang">
                <option @if($data->lang == 'uz') selected  @endif value="uz">UZ</option>
                <option @if($data->lang == 'ru') selected  @endif value="ru">RU</option>
            </select>
        </div>

        <div class="form-group">
            <label for="file">@lang('admin.poster') @if(!is_file($data->poster)) <font color="red"><i class="fas fa-ban" data-toggle="tooltip" data-placement="top" title="@lang('admin.file_not_found')"></i></font> @else <font color="green"><i data-toggle="tooltip" data-placement="top" title="@lang('admin.file_empty')" class="fas fa-check"></i></font> @endif</label>
            <input type="file" name="poster" class="form-control" id="file"  placeholder="@lang('admin.poster')">
        </div>

        <div class="form-group">
            <label for="title">@lang('admin.title')</label>
            <textarea name="full_text" id="editor" height="400">{!! $data->full_text !!}</textarea>
        </div>

        <div class="form-group form-check">
            <input type="checkbox" class="form-check-input" name="published" @if($data->published == 1) checked @endif  value="1" id="publish">
            <label class="form-check-label" for="publish">@lang('admin.published_check')</label>
        </div>

        <button type="submit" class="btn btn-primary"><i class="fas fa-save"></i> @lang('admin.save')</button>

        @csrf
    </form>
@endsection


@push('js')
    <script src="/vendor/ckeditor/ckeditor.js"></script>

    <script>
        ClassicEditor
            .create( document.querySelector( '#editor' ), {
                height: 400
            } )

            .catch( error => {
            console.error( error );
        } );
    </script>

    <script>
        $(function () {
            $('[data-toggle="tooltip"]').tooltip()
        })
    </script>
@endpush