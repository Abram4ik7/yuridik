@extends('dashboard.layouts.app')
@section('title', trans('admin.blogs').' - ')

@section('content')
    <div class="d-flex justify-content-between flex-wrap flex-md-nowrap align-items-center pt-3 pb-2 mb-3 border-bottom">
        <h1 class="h2">@lang('admin.blogs')</h1>

        <div class="btn-toolbar mb-2 mb-md-0">
            <a href="{{Route('d-blogs-add')}}" class="btn btn-success">
                <i class="fas fa-plus-square"></i> @lang('admin.add')
            </a>
        </div>
    </div>

    @if(!empty(Session::get('info')))
        <div class="alert alert-info">
            <i class="fas fa-info-circle"></i> {{Session::get('info')}}
        </div>
    @endif

    <table class="table">
        <thead class="thead-dark">
        <tr>
            <th scope="col">ID</th>
            <th scope="col" class="">@lang('admin.poster')</th>
            <th scope="col">@lang('admin.title')</th>
            <th scope="col">@lang('admin.lang')</th>
            <th scope="col">@lang('admin.published_user')</th>
            <th scope="col">@lang('admin.published')</th>
            <th scope="col">@lang('admin.actions')</th>
        </tr>
        </thead>
        <tbody>
            @foreach($blogs as $blog)
                <tr>
                    <th scope="row">{{$blog->id}}</th>
                    <td class="text-center">
                        @if($blog->poster == null and !is_file($blog->poster))
                            <img src="/img/nophoto.jpg"  width="100" >
                        @else
                            <img src="/{{$blog->poster}}" width="50" >
                        @endif
                    </td>
                    <td>
                        {{$blog->title}}
                        @if($blog->published == 0)
                            <font color="red">
                                <i class="fas fa-info-circle" data-toggle="tooltip" data-placement="top" title="@lang('admin.no_published')"></i>
                            </font>
                        @endif
                    </td>
                    <td class="text-center">
                        {{$blog->lang}}
                    </td>
                    <td>{{$blog->user->first_name}}</td>
                    <td>{{date('d.m.Y, H:i',strtotime($blog->created_at))}}</td>
                    <td>
                        <a href="{{Route('d-blogs-edit',[$blog->id])}}" data-toggle="tooltip" data-placement="top" title="@lang('admin.edit')" class="btn btn-sm btn-primary"><i class="fas fa-edit"></i></a>
                        <a href="{{Route('d-blogs-delete',[$blog->id])}}" data-toggle="tooltip" data-placement="top" title="@lang('admin.delete')"  class="btn btn-sm btn-danger" onclick="return confirm('@lang('admin.perform_delete')')"><i class="fas fa-trash"></i></a>
                    </td>
                </tr>
            @endforeach
        </tbody>
    </table>

    {{$blogs->links()}}

@endsection

@push('js')
    <script>
        $(function () {
            $('[data-toggle="tooltip"]').tooltip()
        })
    </script>
@endpush