@extends('site.layouts.app')

@section('content')
    <section class="section-rotate">
        <div class="section-inner bg-gradient-primary"></div>
        <!--Illustation -->
        <div class="pt-5 position-absolute middle right-0 col-lg-7 col-xl-7 d-none d-lg-block">
            <img alt="" src="img/elem1.png" class="img-fluid">
        </div>
        <!-- SVG shapes background -->
        <div class="bg-absolute-cover bg-size--contain d-flex align-items-center">
            <figure class="w-100">

                <svg preserveAspectRatio="none" x="0px" y="0px" viewBox="0 0 1506.3 578.7" xmlns="http://www.w3.org/2000/svg">
                    <path class="shape-fill-purple" d="M 147.269 295.566 C 147.914 293.9 149.399 292.705 151.164 292.431 L 167.694 289.863 C 169.459 289.588 171.236 290.277 172.356 291.668 L 182.845 304.699 C 183.965 306.091 184.258 307.974 183.613 309.64 L 177.572 325.239 C 176.927 326.905 175.442 328.1 173.677 328.375 L 157.147 330.943 C 155.382 331.217 153.605 330.529 152.485 329.137 L 141.996 316.106 C 140.876 314.714 140.583 312.831 141.228 311.165 L 147.269 295.566 Z"></path>
                    <path class="shape-fill-green" d="M 92.927 474.881 C 93.309 473.896 94.187 473.19 95.23 473.028 L 105.002 471.51 C 106.045 471.347 107.096 471.754 107.758 472.577 L 113.959 480.28 C 114.621 481.103 114.794 482.216 114.413 483.201 L 110.841 492.423 C 110.46 493.408 109.582 494.114 108.539 494.277 L 98.767 495.795 C 97.723 495.957 96.673 495.55 96.011 494.727 L 89.81 487.024 C 89.148 486.201 88.975 485.088 89.356 484.103 L 92.927 474.881 Z"></path>
                    <path class="shape-fill-teal" d="M 34.176 36.897 C 34.821 35.231 36.306 34.036 38.071 33.762 L 54.601 31.194 C 56.366 30.919 58.143 31.608 59.263 32.999 L 69.752 46.03 C 70.872 47.422 71.165 49.305 70.52 50.971 L 64.479 66.57 C 63.834 68.236 62.349 69.431 60.584 69.706 L 44.054 72.274 C 42.289 72.548 40.512 71.86 39.392 70.468 L 28.903 57.437 C 27.783 56.045 27.49 54.162 28.135 52.496 L 34.176 36.897 Z"></path>
                    <path class="shape-fill-blue" d="M 975.636 9.762 C 976.101 8.561 977.171 7.7 978.443 7.502 L 990.354 5.652 C 991.626 5.454 992.907 5.95 993.714 6.953 L 1001.272 16.343 C 1002.079 17.346 1002.29 18.703 1001.826 19.903 L 997.472 31.144 C 997.008 32.344 995.938 33.205 994.666 33.403 L 982.754 35.254 C 981.483 35.451 980.202 34.956 979.395 33.953 L 971.837 24.563 C 971.03 23.559 970.818 22.203 971.283 21.002 L 975.636 9.762 Z"></path>
                    <path class="shape-fill-gray-dark" d="M 1417.759 409.863 C 1418.404 408.197 1419.889 407.002 1421.654 406.728 L 1438.184 404.16 C 1439.949 403.885 1441.726 404.574 1442.846 405.965 L 1453.335 418.996 C 1454.455 420.388 1454.748 422.271 1454.103 423.937 L 1448.062 439.536 C 1447.417 441.202 1445.932 442.397 1444.167 442.672 L 1427.637 445.24 C 1425.872 445.514 1424.095 444.826 1422.975 443.434 L 1412.486 430.403 C 1411.366 429.011 1411.073 427.128 1411.718 425.462 L 1417.759 409.863 Z"></path>
                    <path class="shape-fill-orange" d="M 1313.903 202.809 C 1314.266 201.873 1315.1 201.201 1316.092 201.047 L 1325.381 199.604 C 1326.373 199.449 1327.372 199.837 1328.001 200.618 L 1333.895 207.941 C 1334.525 208.723 1334.689 209.782 1334.327 210.718 L 1330.932 219.484 C 1330.57 220.42 1329.735 221.092 1328.743 221.246 L 1319.454 222.689 C 1318.462 222.843 1317.464 222.457 1316.834 221.674 L 1310.94 214.351 C 1310.31 213.569 1310.146 212.511 1310.508 211.575 L 1313.903 202.809 Z"></path>
                    <path class="shape-fill-red" d="M 1084.395 506.137 C 1084.908 504.812 1086.09 503.861 1087.494 503.643 L 1100.645 501.6 C 1102.049 501.381 1103.463 501.929 1104.354 503.036 L 1112.699 513.403 C 1113.59 514.51 1113.823 516.009 1113.31 517.334 L 1108.504 529.744 C 1107.99 531.07 1106.809 532.02 1105.405 532.239 L 1092.254 534.282 C 1090.85 534.5 1089.436 533.953 1088.545 532.845 L 1080.2 522.478 C 1079.309 521.371 1079.076 519.873 1079.589 518.547 L 1084.395 506.137 Z"></path>
                </svg>


            </figure>
        </div>
        <!-- Hero container -->
        <div class="container pt-lg pb-xl-md position-relative zindex-100">
            <div class="row">
                <div class="col-lg-5 text-center text-lg-left">

                    <div class="">
                        <h2 class="text-white my-4">
                            <span class="display-4 font-weight-light">Onlayn Yuridik  xizmatlar </span>
                        </h2>
                        <p class="lead text-white">Professional  yuristlar xizmatidan onlayn foydalaning. O`z savolingizni  yozing va  1 soat ichida javob oling. Xizmat sifatida kafolatlanadi. Xizmat sizni qanoatlantormasa pulingizni qaytaramiz</p>
                        <div class="mt-5">
                            <a href="https://preview.webpixels.io/purpose-website-ui-kit-v1.1.3/#sct_page_examples" class="btn btn-dark btn-circle btn-translate--hover btn-icon mr-sm-4 scroll-me">
                                <span class="btn-inner--text">Savol bering</span>
                                <span class="btn-inner--icon"><i class="fas fa-angle-right"></i></span>
                            </a>
                            <a href="https://preview.webpixels.io/purpose-website-ui-kit-v1.1.3/#sct_features" class="btn btn-outline-white btn-circle btn-translate--hover btn-icon d-none d-xl-inline-block scroll-me">
                                <span class="btn-inner--icon"><i class="fas fa-file-alt"></i></span>
                                <span class="btn-inner--text">Barcha savollar</span>
                            </a>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>

    <section id="sct_page_examples" class="slice bg_trans bg_first padding-top-10">
        <div class="container">
            <div class="row">
                <div class="col-md-12">
                    <h1 class="margin-bottom-40 strong-600 color_blue">
                        <center>
                            Nega yuridik.uz?
                        </center>
                    </h1>
                </div>
                <div class="col-lg-4 mb-5 mb-lg-0">
                    <div data-animate-hover="1" data-toggle="hidden">
                        <div class="card shadow animate-this">
                            <div class="py-5 text-center">
                                <img alt="" src="img/ico1.png" class="img-saturate" style="width: 100px;">
                            </div>
                            <div class="px-4 pb-5 text-center">
                                <b class="">
                                    Doimo yoningizda!
                                </b>
                                <p class="mt-4">
                                    Istalgan vaqtda va joyda savol bering.
                                    Istagancha savolga qo`shimcha aniqlashtirishlar kiriting va faqat professional yuristlardan javob oling
                                </p>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="col-lg-4 mb-5 mb-lg-0">
                    <div data-animate-hover="1" data-toggle="hidden">
                        <div class="card shadow animate-this">
                            <div class="py-5 text-center">
                                <img alt="" src="img/ico2.png" class="img-saturate" style="width: 100px;">
                            </div>
                            <div class="px-4 pb-5 text-center">
                                <b class="">
                                    Biz kafolat beramiz!
                                </b>
                                <p class="mt-4">
                                    Yuridik.uz - kafolatlangan yuridik xizmat demaktir. sifatli yuridik konsultatsiya berilishini kafolatlaymiz.<br><br>
                                </p>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="col-lg-4 mb-5 mb-lg-0">
                    <div data-animate-hover="1" data-toggle="hidden">
                        <div class="card shadow animate-this">
                            <div class="py-5 text-center">
                                <img alt="" src="img/ico3.png" class="img-saturate" style="width: 100px;">
                            </div>
                            <div class="px-4 pb-5 text-center">
                                <b class="">
                                    Ham arzon, ham tezkor
                                </b>
                                <p class="mt-4">
                                    Savol uchun narxni o`zingiz belgilaysiz. Bir emas, bir necha profesional fikrni biling va albatta biz 24/7 soat xizmat ko`rsatamiz!
                                </p>
                            </div>
                        </div>
                    </div>
                </div>
            </div>

        </div>
    </section>


    <section class="slice slice-xl bg-gradient-primary has-floating-items" id="sct_call_to_action">
        <a href="#sct_call_to_action" class="tongue tongue-up tongue-secondary scroll-me"><i class="far fa fa-angle-up"></i></a>
        <div class="container text-center">
            <div class="row">
                <div class="col-12">
                    <h1 class="text-white strong-700">Bu qanday ishlaydi?</h1>
                    <div class="row justify-content-center mt-4">
                        <div class="col-lg-12">
                            <div class="lead text-white">
                                <div class="col-md-12">
                                    <div class="row">
                                        <div class="col-md-3 col-6 text-center">
                                            <img src="img/ico4.png" width="100%">
                                            <p class="margin-top-10">SAVOL BERING</p>
                                        </div>

                                        <div class="col-md-3 col-6 text-center">
                                            <img src="img/ico5.png" width="100%">
                                            <p class="margin-top-10">JAVOB OLING</p>
                                        </div>

                                        <div class="col-md-3  col-6 text-center">
                                            <img src="img/ico6.png" width="100%">
                                            <p class="margin-top-10">MUAMMO HAL!</p>
                                        </div>

                                        <div class="col-md-3 col-6 text-center">
                                            <img src="img/ico7.png" width="100%">
                                            <p class="margin-top-10">YURIDIK.UZ</p>
                                        </div>
                                    </div>
                                </div>
                            </div>

                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>

    <section class="slice slice-lg pt-md-xl bg-secondary background_questions padding-top-40">
        <div class="container">
            <h1 class="margin-bottom-40 strong-600 color_default">
                <center>

                    Hozirgacha <b>1254</b> savolga javob berildi:


                </center>
            </h1>

            <div class="col-md-12 questions_block">
                <div class="row">
                    <div class="col-md-12 div_center">
                        <div class="btn-group btn-group-justified question_tab_border" role="group" aria-label="...">
                            <div class="btn-group" role="group">
                                <button type="button" class="btn btn-primary btn_pul">Pullik</button>
                            </div>

                            <div class="btn-group" role="group">
                                <button type="button" class="btn btn-default btn_bepul">Bepul</button>
                            </div>
                        </div>
                    </div>

                    <div class="col-md-12 margin-top-20">
                        <div class="tab_questions">
                            <div class="tab_actions tab_active" id="pullik_tab">
                                <div class="col-md-12 questions_item_index">
                                    <div class="row">
                                        <div class="col-md-2 text-left">
                                            Bugun 15:55
                                        </div>
                                        <div class="col-md-2 text-center">
                                            <span class="questions_answers">113 ta javob</span>
                                        </div>
                                        <div class="col-md-6">
                                            Buzulyotgan uylarni o`rniga yangi uy-joy kimlar taminlaydi?
                                        </div>
                                        <div class="col-md-2 text-center question_price_index">
                                            5000 sum
                                        </div>
                                    </div>
                                </div><!--item quetions-->

                                <div class="col-md-12 questions_item_index">
                                    <div class="row">
                                        <div class="col-md-2 text-left">
                                            Bugun 15:55
                                        </div>
                                        <div class="col-md-2 text-center">
                                            <span class="questions_answers">113 ta javob</span>
                                        </div>
                                        <div class="col-md-6">
                                            Buzulyotgan uylarni o`rniga yangi uy-joy kimlar taminlaydi?
                                        </div>
                                        <div class="col-md-2 text-center question_price_index">
                                            5000 sum
                                        </div>
                                    </div>
                                </div><!--item quetions-->


                                <div class="col-md-12 questions_item_index">
                                    <div class="row">
                                        <div class="col-md-2 text-left">
                                            Bugun 15:55
                                        </div>
                                        <div class="col-md-2 text-center">
                                            <span class="questions_answers">113 ta javob</span>
                                        </div>
                                        <div class="col-md-6">
                                            Buzulyotgan uylarni o`rniga yangi uy-joy kimlar taminlaydi?
                                        </div>
                                        <div class="col-md-2 text-center question_price_index">
                                            5000 sum
                                        </div>
                                    </div>
                                </div><!--item quetions-->

                                <div class="col-md-12 questions_item_index">
                                    <div class="row">
                                        <div class="col-md-2 text-left">
                                            Bugun 15:55
                                        </div>
                                        <div class="col-md-2 text-center">
                                            <span class="questions_answers">113 ta javob</span>
                                        </div>
                                        <div class="col-md-6">
                                            Buzulyotgan uylarni o`rniga yangi uy-joy kimlar taminlaydi?
                                        </div>
                                        <div class="col-md-2 text-center question_price_index">
                                            5000 sum
                                        </div>
                                    </div>
                                </div><!--item quetions-->

                                <div class="col-md-12 questions_item_index">
                                    <div class="row">
                                        <div class="col-md-2 text-left">
                                            Bugun 15:55
                                        </div>
                                        <div class="col-md-2 text-center">
                                            <span class="questions_answers">113 ta javob</span>
                                        </div>
                                        <div class="col-md-6">
                                            Buzulyotgan uylarni o`rniga yangi uy-joy kimlar taminlaydi?
                                        </div>
                                        <div class="col-md-2 text-center question_price_index">
                                            5000 sum
                                        </div>
                                    </div>
                                </div><!--item quetions-->

                                <div class="col-md-12 questions_item_index">
                                    <div class="row">
                                        <div class="col-md-2 text-left">
                                            Bugun 15:55
                                        </div>
                                        <div class="col-md-2 text-center">
                                            <span class="questions_answers">113 ta javob</span>
                                        </div>
                                        <div class="col-md-6">
                                            Buzulyotgan uylarni o`rniga yangi uy-joy kimlar taminlaydi?
                                        </div>
                                        <div class="col-md-2 text-center question_price_index">
                                            5000 sum
                                        </div>
                                    </div>
                                </div><!--item quetions-->

                                <div class="col-md-12 questions_item_index">
                                    <div class="row">
                                        <div class="col-md-2 text-left">
                                            Bugun 15:55
                                        </div>
                                        <div class="col-md-2 text-center">
                                            <span class="questions_answers">113 ta javob</span>
                                        </div>
                                        <div class="col-md-6">
                                            Buzulyotgan uylarni o`rniga yangi uy-joy kimlar taminlaydi?
                                        </div>
                                        <div class="col-md-2 text-center question_price_index">
                                            5000 sum
                                        </div>
                                    </div>
                                </div><!--item quetions-->

                                <div class="col-md-12 questions_item_index">
                                    <div class="row">
                                        <div class="col-md-2 text-left">
                                            Bugun 15:55
                                        </div>
                                        <div class="col-md-2 text-center">
                                            <span class="questions_answers">113 ta javob</span>
                                        </div>
                                        <div class="col-md-6">
                                            Buzulyotgan uylarni o`rniga yangi uy-joy kimlar taminlaydi?
                                        </div>
                                        <div class="col-md-2 text-center question_price_index">
                                            5000 sum
                                        </div>
                                    </div>
                                </div><!--item quetions-->

                                <div class="col-md-12 questions_item_index">
                                    <div class="row">
                                        <div class="col-md-2 text-left">
                                            Bugun 15:55
                                        </div>
                                        <div class="col-md-2 text-center">
                                            <span class="questions_answers">113 ta javob</span>
                                        </div>
                                        <div class="col-md-6">
                                            Buzulyotgan uylarni o`rniga yangi uy-joy kimlar taminlaydi?
                                        </div>
                                        <div class="col-md-2 text-center question_price_index">
                                            5000 sum
                                        </div>
                                    </div>
                                </div><!--item quetions-->

                                <center>
                                    <a href="#" class="btn btn-primary margin-top-20">
                                        Barcha savollar
                                    </a>
                                </center>
                            </div><!--pullik tab-->

                            <div class="tab_actions" id="bepul_tab">
                                2
                            </div>

                        </div>
                    </div>
                </div>
            </div><!--questions tab-->
        </div>
    </section>

    <section class="slice slice-lg maqollar_bg" id="maqollar">
        <a href="#maqollar" class="tongue tongue-up tongue-secondary scroll-me"><i class="fas fa-angle-up"></i></a>
        <div class="container">

            <div class="col-12">
                <center>
                    <h1 class="text-white strong-700 ">Maqolalar</h1>
                </center>
                <a class="float-left cor_left m_prev myclass01" href="#" ><i class="fa fa-angle-left"></i></a>
                <a class="float-right cor_right m_next myclass01" href="#" ><i class="fa fa-angle-right"></i></a>

                <div class="col-12">



                    <!-- Swiper -->
                    <div class="swiper-container maqollar_slider">
                        <div class="swiper-wrapper">
                            <div class="swiper-slide">
                                <div class="maqolalar_item">
                                    <div class="top-img" style="background: url(images/noavatar.png);"></div>
                                    <span class="mql_title">
                                                <b>Sud xarajatlari nimalardan iborat?</b>
                                            </span>
                                    <hr>
                                    <div class="mql_details">
                                        <i class="fa fa-calendar"></i> 01.11.2018

                                        <span class="float-right text-right"> <i class="fa fa-eye"></i> 123</span>
                                    </div>
                                    <hr>

                                    <p>AQSh universitetlarining biriga huquqshunoslar uchun magistrlik dasturiga (LL.M. ...</p>
                                    <a href="#" class="mql_a">Batafsil o`qish</a>
                                </div>
                            </div> <!--item maqollar-->

                            <div class="swiper-slide">
                                <div class="maqolalar_item">
                                    <div class="top-img" style="background: url(img/img1.JPG);"></div>
                                    <span class="mql_title">
                                              <b>Sud xarajatlari nimalardan iborat?</b>
                                            </span>
                                    <hr>
                                    <div class="mql_details">
                                        <i class="fa fa-calendar"></i> 01.11.2018

                                        <span class="float-right text-right"> <i class="fa fa-eye"></i> 123</span>
                                    </div>
                                    <hr>

                                    <p>AQSh universitetlarining biriga huquqshunoslar uchun magistrlik dasturiga (LL.M. ...</p>
                                    <a href="#" class="mql_a">Batafsil o`qish</a>
                                </div>
                            </div> <!--item maqollar-->

                            <div class="swiper-slide">
                                <div class="maqolalar_item">
                                    <div class="top-img" style="background: url(img/img2.JPG);"></div>
                                    <span class="mql_title">
                                               <b>Sud xarajatlari nimalardan iborat?</b>
                                            </span>
                                    <hr>
                                    <div class="mql_details">
                                        <i class="fa fa-calendar"></i> 01.11.2018

                                        <span class="float-right text-right"> <i class="fa fa-eye"></i> 123</span>
                                    </div>
                                    <hr>

                                    <p>AQSh universitetlarining biriga huquqshunoslar uchun magistrlik dasturiga (LL.M. ...</p>
                                    <a href="#" class="mql_a">Batafsil o`qish</a>
                                </div>
                            </div> <!--item maqollar-->


                            <div class="swiper-slide">
                                <div class="maqolalar_item">
                                    <div class="top-img" style="background: url(img/img3.JPG);"></div>
                                    <span class="mql_title">
                                               <b>Sud xarajatlari nimalardan iborat?</b>
                                            </span>
                                    <hr>
                                    <div class="mql_details">
                                        <i class="fa fa-calendar"></i> 01.11.2018

                                        <span class="float-right text-right"> <i class="fa fa-eye"></i> 123</span>
                                    </div>
                                    <hr>

                                    <p>AQSh universitetlarining biriga huquqshunoslar uchun magistrlik dasturiga (LL.M. ...</p>
                                    <a href="#" class="mql_a">Batafsil o`qish</a>
                                </div>
                            </div> <!--item maqollar-->
                        </div>
                        <!-- Add Arrows -->
                    </div>


                    <a href="maqolalar.html" class="mybtn">Barcha maqolalar</a>



                </div>
            </div>

        </div>
    </section>


    <section id="our_yurist" class="slice bg_trans bg_first padding-top-10">
        <a href="#our_yurist" class="tongue  tongue-up tongue-secondary tongue-353A40 scroll-me"><i class="far fa fa-angle-up" style="color:white !important;"></i></a>
        <br /> <br />
        <div class="container">
            <div class="row">
                <div class="col-md-12">
                    <h1 class="margin-bottom-40 strong-600 color_blue">
                        <center>
                            <font style="vertical-align: inherit;">
                                Bizning huquqshunoslar                            </font>
                        </center>
                    </h1>
                </div>
                <div class="col-md-12 slider_el padding0">
                    <ul>
                        <li class="float-right lw_next"><a href="#"><i class="fa fa-angle-right"></i></a></li>
                        <li class="float-right lw_prev"><a href="#"><i class="fa fa-angle-left"></i></a></li>
                    </ul>
                </div>
                <!-- Swiper -->
                <div class="col-md-12" style="margin-bottom: 50px">
                    <div class="swiper-container lawyers_slider">
                        <div class="swiper-wrapper">
                            <div class="swiper-slide">
                                <a href="#" class="w-100 h-100">
                                    <div class="maqolalar_item  p-4 pb-5 shadow">

                                        <div class="text-center">
                                            <img class="rounded-circle myimg1"  src="images/noavatar.png" >
                                        </div>
                                        <span class="mql_title">
                                                   <b>lawyer lawyer</b>
                                               </span>
                                        <p>Toshkent shahar</p>
                                        <hr>
                                        <span class="float-left text-success">
                                                28
                                            </span>
                                        <p class="float-left mb-0 mt-1 ml-1 text-success"> minnatdorchilik</p>



                                    </div>
                                </a>
                            </div>

                            <div class="swiper-slide">
                                <a href="#" class="w-100 h-100">
                                    <div class="maqolalar_item  p-4 pb-5 shadow">

                                        <div class="text-center">
                                            <img class="rounded-circle myimg1"  src="images/noavatar.png" >
                                        </div>
                                        <span class="mql_title">
                                               <b>lawyer lawyer</b>
                                           </span>
                                        <p>Toshkent shahar</p>
                                        <hr>
                                        <span class="float-left text-success">
                                            28
                                        </span>
                                        <p class="float-left mb-0 mt-1 ml-1 text-success"> minnatdorchilik</p>



                                    </div>
                                </a>
                            </div>

                            <div class="swiper-slide">
                                <a href="#" class="w-100 h-100">
                                    <div class="maqolalar_item  p-4 pb-5 shadow">

                                        <div class="text-center">
                                            <img class="rounded-circle myimg1"  src="images/noavatar.png" >
                                        </div>
                                        <span class="mql_title">
                                           <b>lawyer lawyer</b>
                                       </span>
                                        <p>Toshkent shahar</p>
                                        <hr>
                                        <span class="float-left text-success">
                                        28
                                    </span>
                                        <p class="float-left mb-0 mt-1 ml-1 text-success"> minnatdorchilik</p>



                                    </div>
                                </a>
                            </div>

                            <div class="swiper-slide">
                                <a href="#" class="w-100 h-100">
                                    <div class="maqolalar_item  p-4 pb-5 shadow">

                                        <div class="text-center">
                                            <img class="rounded-circle myimg1"  src="images/noavatar.png" >
                                        </div>
                                        <span class="mql_title">
                                       <b>lawyer lawyer</b>
                                   </span>
                                        <p>Toshkent shahar</p>
                                        <hr>
                                        <span class="float-left text-success">
                                    28
                                </span>
                                        <p class="float-left mb-0 mt-1 ml-1 text-success"> minnatdorchilik</p>



                                    </div>
                                </a>
                            </div>

                            <div class="swiper-slide">
                                <a href="#" class="w-100 h-100">
                                    <div class="maqolalar_item  p-4 pb-5 shadow">

                                        <div class="text-center">
                                            <img class="rounded-circle myimg1"  src="images/noavatar.png" >
                                        </div>
                                        <span class="mql_title">
                                   <b>lawyer lawyer</b>
                               </span>
                                        <p>Toshkent shahar</p>
                                        <hr>
                                        <span class="float-left text-success">
                                28
                            </span>
                                        <p class="float-left mb-0 mt-1 ml-1 text-success"> minnatdorchilik</p>



                                    </div>
                                </a>
                            </div>

                            <div class="swiper-slide">
                                <a href="#" class="w-100 h-100">
                                    <div class="maqolalar_item  p-4 pb-5 shadow">

                                        <div class="text-center">
                                            <img class="rounded-circle myimg1"  src="images/noavatar.png" >
                                        </div>
                                        <span class="mql_title">
                               <b>lawyer lawyer</b>
                           </span>
                                        <p>Toshkent shahar</p>
                                        <hr>
                                        <span class="float-left text-success">
                            28
                        </span>
                                        <p class="float-left mb-0 mt-1 ml-1 text-success"> minnatdorchilik</p>



                                    </div>
                                </a>
                            </div>

                            <div class="swiper-slide">
                                <a href="#" class="w-100 h-100">
                                    <div class="maqolalar_item p-4 pb-5 shadow">

                                        <div class="text-center">
                                            <img class="rounded-circle myimg1"  src="images/noavatar.png" >
                                        </div>
                                        <span class="mql_title">
                           <b>lawyer lawyer</b>
                       </span>
                                        <p>Toshkent shahar</p>
                                        <hr>
                                        <span class="float-left text-success">
                        28
                    </span>
                                        <p class="float-left mb-0 mt-1 ml-1 text-success"> minnatdorchilik</p>



                                    </div>
                                </a>
                            </div>

                            <div class="swiper-slide">
                                <a href="#" class="w-100 h-100">
                                    <div class="maqolalar_item p-4 pb-5 shadow">

                                        <div class="text-center">
                                            <img class="rounded-circle myimg1"  src="images/noavatar.png" >
                                        </div>
                                        <span class="mql_title">
                       <b>lawyer lawyer</b>
                   </span>
                                        <p>Toshkent shahar</p>
                                        <hr>
                                        <span class="float-left text-success">
                    28
                </span>
                                        <p class="float-left mb-0 mt-1 ml-1 text-success"> minnatdorchilik</p>



                                    </div>
                                </a>
                            </div>




                        </div>
                        <!-- Add Pagination -->
                    </div>
                </div>
                <div class="col-md-12">

                    <a href="lawyers.html" class="mybtn">Barcha huquqshunoslar</a>

                </div>
            </div>
        </div>
    </section>




    @include('site.includes.footer')
@endsection
