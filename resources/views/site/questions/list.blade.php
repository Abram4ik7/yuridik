@extends('site.layouts.app')
@section('title', trans('site.questions.title').' - ')
@section('content')
    <section class="slice slice-lg bg-gradient-primary">
        <div class="container pt-lg">
            <div class="row justify-content-center">
                <div class="col-lg-9">
                    <form>
                        <div class="form-group bg-white rounded px-2 py-2 shadow">
                            <div class="row myclass1">
                                <div class="col-md-8">
                                    <div class="input-group input-group-transparent shadow-none">
                                        <div class="input-group-prepend">
                                            <span class="input-group-text border-0"><i class="fa fa-search"></i></span>
                                        </div>
                                        <input type="text" class="form-control border-0 shadow-none"
                                               placeholder="@lang('site.search')">
                                    </div>
                                </div>
                                <div class="col-md-4 text-right">
                                    <button type="button" class="btn btn-primary btn-icon">
                                        <span class="btn-inner--icon"><i class="fa fa-search"></i></span>
                                        <span class="btn-inner--text">@lang('site.search')</span>
                                    </button>
                                </div>
                            </div>
                        </div>
                    </form>
                </div>
            </div>
        </div>
        <a href="#sct-article" class="tongue tongue-bottom tongue-white scroll-me">
            <i class="fa fa-angle-down"></i>
        </a>
    </section>


    <section class="slice" id="sct-article">
        <div class="container">
            <div class="text-center mb-5">
                <h1 class=" margin-bottom-40 strong-600 color_blue">
                    <center>
                        @lang('site.questions.title')
                    </center>
                </h1>
            </div>
            <div class="row">
                <div class="col-md-8 col-lg-8 float-left col-xl-8 question_block">
                    <ul style="padding-left: inherit;">
                        <li class="btn-all @if(is_active(route('questions'))) btn-active @endif" id="btn-all">
                            <a href="{{ Route('questions') }}">@lang('site.questions.all')</a>
                        </li>
                        <li class="btn-money  @if(is_active(route('questions.paid'))) btn-active @endif " id="btn-money">
                            <a href="{{ Route('questions.paid') }}">@lang('site.questions.paid')</a>
                        </li>
                        <li class="btn-free  @if(is_active(route('questions.free'))) btn-active @endif" id="btn-free">
                            <a href="{{ Route('questions.free') }}">@lang('site.questions.free')</a>
                        </li>
                    </ul>
                    <div class="questions_div">
                        @foreach($questions as $question)
                            <div class="q-status @if($question->top_question) top_question @endif  hover-q col-md-12 col-lg-12 col-xl-12 pt-3 float-left pb-3 ">
                                @if($question->price > 0)
                                    <div class="top_paid">
                                        <span class="question_price">
                                            <b>{{ number_format($question->price,0,'',' ') }} @lang('site.ye')</b>
                                            <span>
                                                @lang('site.questions.price')
                                            </span>
                                        </span>
                                        @if($question->top_question)
                                            <span class="croaked_question">
                                                    <i class="fas fa-paperclip"></i> @lang('site.questions.top')
                                            </span>
                                        @endif
                                    </div>
                                @endif
                                <p class="d-block">
                                    <b>
                                        <a class="hover-a-q" href="{{ Route('questions.view',[ $question->id, $question->slug ]) }}">
                                            {{ $question->title }}
                                        </a>
                                    </b>
                                </p>
                                <p>
                                    {{ Str::limit(strip_tags($question->full_text), 300) }}
                                </p>
                                @if(Session::has('msg'.$question->id))
                                    <div class="shadow margin-bottom-20  bg-white col-md-12 float-left question-nat"  id="nat-{{ $question->id }}" >
                                        <div class="col-md-2 pt-3 pb-3 float-left">
                                            <i class="fa fa-lock " style="color:blue !important;font-size:100px "></i>
                                        </div>
                                        <div class="col-md-10 pt-3 pb-3 float-left">
                                            <a class="d-block"><b>@lang('site.questions.access_to_this_question_is_limited')</b></a>
                                            <small class="d-block">@lang('site.questions.limited_reason')</small>
                                            <small class="d-block">@lang('site.questions.limited_reason_one')</small>
                                            <small class="d-block">@lang('site.questions.limited_reason_two')</small>
                                        </div>
                                    </div>
                                @endif
                                <span class="float-left">
                                        <i class="fa fa-user"></i> <span class="span-size">{{ $question->users->first_name }} {{ $question->users->last_name }}</span><br>
                                        <i class="fa fa-calendar"></i> <small>{{ date('d.m.Y, H:i', strtotime($question->updated_at)) }}</small>
                                    </span>
                                <span class="float-right">
                                    <b><i class="fa fa-comments"></i> @lang('site.questions.answers')</b>
                                    <span class="q-circle">{{ $question->answersCount() }}</span><br>
                                    <i class="fa fa-map-marker"></i> {{ $question->users->city->name() }}
                                </span>

                                @if(auth()->check() && $question->service_id == 3 && auth()->user()->isLegalCounsel() || auth()->check() && auth()->user()->isReply())
                                    <div class="col-md-12 mt-3 float-left padding0">
                                        <div class="row" id="reply_view{{$question->id}}">
                                            <input class="form-control reply"  id="reply{{$question->id}}" data-id="{{$question->id}}" type="text" placeholder="@lang('site.questions.answer_reply')">
                                            <div id="reply_div{{$question->id}}" class="w-100" style="display: none">
                                                <textarea id="reply_text{{$question->id}}"></textarea>

                                                <div class="text-right margin-top-20">

                                                    <button class="btn btn-default btn-xs btn_refusal" data-id="{{$question->id}}">
                                                        @lang('site.questions.otmen')
                                                    </button>

                                                    <button class="btn btn-primary btn-xs btn_send" data-id="{{$question->id}}">
                                                        <i class="fas fa-comment"></i> @lang('site.questions.answer_reply')
                                                    </button>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                @endif

                            </div>
                        @endforeach
                    </div>{{--questions div--}}

                    <div class="col-md-12 col-lg-12 col-xl-12 mt-5 text-center float-left">
                        <center>
                            {{ $questions->links() }}
                        </center>
                    </div>
                </div>
                <div class="col-lg-4">
                    <h3 class="text-center">
                        @lang('site.active_lawyers')
                    </h3>
                    @foreach($randLawyers as $lawyer)
                        <div class="col-md-12 mb-3 p-0">
                            <div class="mycard shadow ">
                                <a href="{{ Route('users-view',[ $lawyer->id, str_slug($lawyer->last_name) ]) }}" class="w-100 h-100 p-3 d-flex">
                                    @if($lawyer->photo && is_file($lawyer->photo))
                                        <img class="rounded-circle myimg2" src="/{{ $lawyer->photo }}"
                                             alt="{{ $lawyer->first_name }} {{ $lawyer->last_name }}">
                                    @else
                                        <img class="rounded-circle myimg2" src="/images/noavatar.png"
                                             alt="{{ $lawyer->first_name }} {{ $lawyer->last_name }}">
                                    @endif

                                    <div class="d-flex ml-3 flex-column">
                                         <span class="mql_title">
                                           <b>{{ $lawyer->first_name }} {{ $lawyer->last_name }}</b>
                                         </span>
                                        <p class="mb-0" style="font-size: 12px;">
                                            {{ $lawyer->city->name() }}
                                        </p>
                                        <hr>
                                        <div class="d-flex">
                                           <span class="text-success">
                                            {{ $lawyer->thanks }}
                                        </span>
                                            <p class="mb-0 mt-0 ml-1 text-success"> @lang('site.users.thanks')</p>
                                        </div>
                                    </div>
                                    <a href="{{ Route('users-view',[ $lawyer->id, str_slug($lawyer->last_name) ]) }}" class="mybtn2">@lang('site.more')</a>
                                </a>
                            </div>
                        </div>
                    @endforeach
                </div>
            </div>

        </div>
    </section>

    @include('site.includes.footer')
@endsection

@push('js')
    <script src="/vendor/tinymce/tinymce.min.js"></script>

    <script>
        $('.reply').on('click',function(e) {
            e.preventDefault;
            var id = $(this).data('id');
            $(this).hide();

            $( '#reply_div'+id ).slideDown( "slow", function() {
                $(this).show();
            });

            tinymce.init({
                selector: "#reply_text" + id,  // change this value according to your HTML
                menubar:false,
                statusbar: false,
                plugins: "lists link paste ",
                toolbar: 'undo redo|  bold italic | alignleft | bullist numlist | blockquote | link | paste',
                width: '100%',
                height: 300,
            });
        })


        $('.btn_refusal').on('click', function(e) {
            e.preventDefault;
            var id = $(this).data('id');

            hideReply(id);

        })

        function hideReply(id)
        {
            $( "#reply_div"+id ).slideUp( "slow", function() {
                $(this).hide()
                tinymce.remove("#reply_text" + id);
                $('#reply'+id).show();
            });
        }

        $('.btn_send').on('click', function(e) {
            var id = $(this).data('id');
            var text = tinymce.activeEditor.getContent({format: 'raw'});

            $.ajax({
                url: '{{ Route('questions.ajax_reply') }}',
                method: "POST",
                data: {_token: "{{csrf_token()}}", id: id, text:text}
            }).done(function(data) {
                if(data.status == true) {
                    hideReply(id);
                    $('#reply'+id).hide();
                    $('#reply_view' + id).html("<div class='alert alert-success w-100'><i class='fas fa-check'></i> @lang("site.questions.success_reply")</div>")
                }
            });
        });
    </script>
@endpush
