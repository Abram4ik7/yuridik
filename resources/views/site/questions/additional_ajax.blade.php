<h4 class="margin-top-20 float-left w-100">
    <span>
        @lang('site.questions.additional')
    </span>
</h4>

{!! $q->full_text !!}

@if(count($q->additional_files) > 0)
    <div class="margin-bottom-20 float-left w-100">
        @foreach($q->additional_files as $file)
            <div class="col-md-12 float-left ">
                <div class="row">
                    <a href="{{ Route('download_file', [ $file->id ]) }}" class="col-md-5 padding0">
                        <div class="col-md-12 download-file shadow  float-left margin-top-10">
                            <div class="row">
                                <div class="col-md-2 float-left">
                                    <svg class="svg-icon svg-icon--svg_icon_staple"><use xmlns:xlink="http://www.w3.org/1999/xlink" xlink:href="/assets/extension/icons.svg#svg-icon-staple"></use></svg>
                                </div>

                                <div class="col-md-8 download-file-name float-left padding0">
                                    <span class="color_blue">
                                        {{ Str::limit($file->name, 5,'...',5) }}
                                    </span>
                                    <span class="float-right">
                                        {{round($file->filesize / 1024)}} kbayt
                                    </span>
                                </div>

                                <div class="col-md-2 float-left">
                                    <svg class="svg-icon svg-icon--download"><use xmlns:xlink="http://www.w3.org/1999/xlink" xlink:href="/assets/extension/icons.svg#svg-icon-download"></use></svg>
                                </div>
                            </div>
                        </div>
                    </a>
                </div>
            </div>
        @endforeach
    </div>
@endif

<div class="col-md-12 w-100 padding0 float-left">
    <i class="fas fa-calendar"></i> {{ date('d.m.Y, H:i', strtotime($q->created_at)) }}
</div>