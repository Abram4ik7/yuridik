@extends('site.layouts.app')
@section('title', $question->title.' - ')
@section('content')
    <section class="slice slice-lg bg-gradient-primary">
        <a href="#sct-article" class="tongue tongue-bottom tongue-white scroll-me">
            <i class="fa fa-angle-down"></i>
        </a>
    </section>

    <section class="slice" id="sct-article">
        <div class="container ">
            <div class="row justify-content-center">
                <div class="col-md-12 col-lg-12 col-xl-12">
                    <h3>
                        @lang('site.question'):
                        @if($question->type_user == 1)
                            @lang('site.questions.person')
                        @elseif($question->type_user == 2)
                            @lang('site.questions.legal_person')
                        @endif

                        <a href="javascript:history.back()" class="float-right" >
                            <i class="fa fa-mail-reply-all"></i>
                        </a>
                    </h3>
                    <div class="shadow card p-2 shadow zindex-100 ">
                        <div class="card-body">
                            @if($question->price > 0)
                                <div class="top_paid">
                                        <span class="question_price question_price_in">
                                            <b>{{ number_format($question->price,0,'',' ') }} @lang('site.ye')</b>
                                            <span>
                                                @lang('site.questions.price')
                                            </span>
                                        </span>
                                    @if($question->top_question)
                                        <span class="croaked_question">
                                            <i class="fas fa-paperclip"></i> @lang('site.questions.top')
                                        </span>
                                    @endif
                                </div>
                            @endif
                            <h4>
                                <span>
                                    {{ $question->title }}
                                </span>
                            </h4>

                            {!! $question->full_text !!}

                            @if($question->files)
                                <div class="margin-bottom-20 float-left w-100">
                                    @foreach($question->files as $file)
                                        <div class="col-md-12 float-left ">
                                            <div class="row">
                                                <a href="{{ Route('download_file', [ $file->id ]) }}" class="col-md-5 padding0">
                                                    <div class="col-md-12 download-file shadow  float-left margin-top-10">
                                                        <div class="row">
                                                            <div class="col-md-2 float-left">
                                                                <svg class="svg-icon svg-icon--svg_icon_staple"><use xmlns:xlink="http://www.w3.org/1999/xlink" xlink:href="/assets/extension/icons.svg#svg-icon-staple"></use></svg>
                                                            </div>

                                                            <div class="col-md-8 download-file-name float-left padding0">
                                                        <span class="color_blue">
                                                            {{ $file->substr() }}
                                                        </span>
                                                                <span class="float-right">
                                                            {{round($file->filesize / 1024)}} kbayt
                                                        </span>
                                                            </div>

                                                            <div class="col-md-2 float-left">
                                                                <svg class="svg-icon svg-icon--download"><use xmlns:xlink="http://www.w3.org/1999/xlink" xlink:href="/assets/extension/icons.svg#svg-icon-download"></use></svg>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </a>
                                            </div>
                                        </div>
                                    @endforeach
                                </div>
                            @endif

                            <div id="additional_show @if(count($question->additionals) > 0) margin-bottom-20 @endif">
                                @foreach($question->additionals as $additional)
                                    @include('site.questions.additional_ajax', [ 'q' => $additional ])
                                @endforeach
                            </div>


                            <label class="d-block margin-bottom-0 @if(count($question->additionals) > 0) w-100 float-left margin-top-20 @endif ">
                                <span>
                                    <i class="fa fa-user"></i> {{ $question->users->first_name }} {{ $question->users->last_name }}
                                </span>
                                <span class="float-right">
                                    <i class="fa fa-calendar"></i> {{ date('d.m.Y, H:i', strtotime($question->updated_at)) }}
                                </span>
                            </label>
                            <label class="d-block margin-bottom-0">
                                <small>
                                    <i class="fa fa-map-marker"></i>
                                    {{ $question->users->city->name() }}
                                </small>
                                <small class="float-right"><i class="fa fa-question-circle"></i> @lang('site.question') №{{ $question->id }}</small>
                            </label>
                        </div>

                        <div class="card-footer">
                            <span>
                                <b><i class="fa fa-tags"></i> @lang('site.questions.speciality'): </b>
                            </span>

                            {{ $question->speciality->name() }}

                            <div class="float-right">
                                @if(auth()->check() && $question->user_id == auth()->user()->id && $question->service_id != 3)
                                    <button data-toggle="collapse" data-target="#collapseadded" aria-expanded="false" aria-controls="collapseadded" data-id="{{ $question->id }}" type="button" class="btn btn-sm btn-default clp_q" style="margin: -8px 10px 0 0;">
                                        <i class="fa fa-plus"></i> @lang('site.questions.added_question')
                                    </button>
                                @endif

                                <button data-toggle="collapse" data-target="#collapseShare" aria-expanded="false" aria-controls="collapseShare" type="button" class="btn btn-sm btn-default" style="margin: -8px 10px 0 0;">
                                    <i class="fa fa-share-alt"></i> @lang('site.questions.share')
                                </button>

                                <span class="float-right">
                                    <b><i class="fa fa-comments"></i> @lang('site.questions.answers')</b>
                                    <span class="q-circle">{{ $question->answersCount() }}</span>
                                </span>
                            </div>
                        </div>

                        <div class="collapse" id="collapseShare">
                            <div class="col-md-12">
                                <div class="addthis_inline_share_toolbox"></div>
                            </div>
                        </div>
                        @if(auth()->check() && $question->user_id == auth()->user()->id && $question->service_id != 3)
                            <div class="collapse" id="collapseadded">
                                <div class="col-md-12">
                                    <h2>@lang('site.questions.added_question_title')</h2>
                                    <form action="{{ Route('questions.additional', [ $question->id ]) }}" id="additional_form" method="post" enctype="multipart/form-data">
                                        <div class="alert alert-danger" id="alert_div" style="display: none;">
                                            <i class="fa fa-info-circle"></i> @lang('site.information')<br>
                                            <div id="reply_alert">

                                            </div>
                                        </div>

                                        <textarea name="full_text" id="reply_add{{$question->id}}"></textarea>
                                        @csrf
                                        <div class="col-md-12 margin-top-20">
                                            <div class="row">
                                                <div class="col-md-6">
                                                    <div class="row">
                                                        <input type="file" name="files[]"  accept="application/pdf, image/* , .doc, .docs, .rtf, .odt, .txt, .xls, .xlsx" id="file-3" class="inputfile inputfile-3" data-multiple-caption="{count} @lang("site.questions.selected_files")" multiple />
                                                        <label for="file-3">
                                                            <svg xmlns="http://www.w3.org/2000/svg" width="20" height="17" viewBox="0 0 20 17"><path d="M10 0l-5.2 4.9h3.3v5.1h3.8v-5.1h3.3l-5.2-4.9zm9.3 11.5l-3.2-2.1h-2l3.4 2.6h-3.5c-.1 0-.2.1-.2.1l-.8 2.3h-6l-.8-2.2c-.1-.1-.1-.2-.2-.2h-3.6l3.4-2.6h-2l-3.2 2.1c-.4.3-.7 1-.6 1.5l.6 3.1c.1.5.7.9 1.2.9h16.3c.6 0 1.1-.4 1.3-.9l.6-3.1c.1-.5-.2-1.2-.7-1.5z"/></svg>
                                                            <span>@lang('site.questions.choose_file')</span>
                                                        </label>
                                                    </div>
                                                </div>


                                                <div class="col-md-6">
                                                    <div class="row float-right">
                                                        <button class="btn btn-default btn-xs btn_refusal_q" data-id="{{$question->id}}">
                                                            @lang('site.questions.otmen')
                                                        </button>

                                                        <button type="submit" class="btn btn-primary btn-xs btn_additional" data-id="{{$question->id}}">
                                                            <i class="fas fa-comment"></i> @lang('site.questions.answer_reply')
                                                        </button>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </form>

                                </div>
                            </div>
                        @endif

                        @if(auth()->check() && $question->service_id == 3 && auth()->user()->isLegalCounsel() ||auth()->check() && auth()->user()->isReply())
                            <div class="col-md-12 mt-3 float-left margin-bottom-20">
                                <div class="alert alert-success" id="success_alert_div" style="display: none;">
                                    <i class="fas fa-info-circle"></i> @lang('site.information')<br>
                                    <div id="success_answered">

                                    </div>
                                </div>
                                <input class="form-control reply" id="reply{{$question->id}}" data-id="{{$question->id}}" type="text" placeholder="@lang('site.questions.answer_reply')">

                                <div id="reply_div{{$question->id}}" class="w-100" style="display: none">
                                    <div class="alert alert-danger" id="alert_div" style="display: none;">
                                        <i class="fa fa-info-circle"></i> @lang('site.information')<br>
                                        <div id="reply_alert">

                                        </div>
                                    </div>

                                    <form action="{{ Route('questions.answer-in', [ $question->id ]) }}" id="reply_form" method="post" enctype="multipart/form-data">
                                        <textarea name="full_text" id="reply_text{{$question->id}}"></textarea>
                                        @csrf
                                        <div class="col-md-12 margin-top-20">
                                            <div class="row">
                                                <div class="col-md-6">
                                                    <div class="row">
                                                        <input type="file" name="files[]"  accept="application/pdf, image/* , .doc, .docs, .rtf, .odt, .txt, .xls, .xlsx" id="file-3" class="inputfile inputfile-3" data-multiple-caption="{count} @lang("site.questions.selected_files")" multiple />
                                                        <label for="file-3">
                                                            <svg xmlns="http://www.w3.org/2000/svg" width="20" height="17" viewBox="0 0 20 17"><path d="M10 0l-5.2 4.9h3.3v5.1h3.8v-5.1h3.3l-5.2-4.9zm9.3 11.5l-3.2-2.1h-2l3.4 2.6h-3.5c-.1 0-.2.1-.2.1l-.8 2.3h-6l-.8-2.2c-.1-.1-.1-.2-.2-.2h-3.6l3.4-2.6h-2l-3.2 2.1c-.4.3-.7 1-.6 1.5l.6 3.1c.1.5.7.9 1.2.9h16.3c.6 0 1.1-.4 1.3-.9l.6-3.1c.1-.5-.2-1.2-.7-1.5z"/></svg>
                                                            <span>@lang('site.questions.choose_file')</span>
                                                        </label>
                                                    </div>
                                                </div>


                                                <div class="col-md-6">
                                                    <div class="row float-right">
                                                        <button class="btn btn-default btn-xs btn_refusal" data-id="{{$question->id}}">
                                                            @lang('site.questions.otmen')
                                                        </button>

                                                        <button type="submit" class="btn btn-primary btn-xs btn_send" data-id="{{$question->id}}">
                                                            <i class="fas fa-comment"></i> @lang('site.questions.answer_reply')
                                                        </button>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </form>
                                </div>
                            </div>
                        @endif

                        @if($question->service_id != 3 && auth()->check() && auth()->user()->isLegalCounsel())
                            <div class="alert alert-warning">
                                <i class="fas fa-info-circle"></i> @lang('site.questions.isNotReplyCounsel')
                            </div>
                        @endif

                    </div>
                    <h3 class="mt-5">@lang('site.questions.answers')</h3>
                    @if(count($answers) == 0)
                        @if(auth()->check() && auth()->user()->id == $question->user_id)
                                <div class="not_answers text-center">
                                    <img src="/assets/images/circular-clock.png" class="margin-bottom-20" width="128">
                                    <br>
                                    <b>
                                        @lang('site.questions.not_answers')
                                    </b>
                                </div>
                        @else
                            <div class="alert alert-info">
                                <i class="fas fa-info-circle"></i> @lang('site.questions.not_answer_guest')
                            </div>
                        @endif
                    @else
                        <div id="answers_append">
                            @include('site.questions.answersTemplate.answersTemplate', ['answers' => $answers])
                            @if($question->service_id != 3)
                                @if(auth()->guest() || auth()->check() && !auth()->user()->isReply())
                                    <div class="alert alert-info float-left w-100  margin-top-20">
                                        <i class="fas fa-info-circle"></i> @lang('site.information') @lang('site.questions.not_view_answers')
                                    </div>
                                @endif
                            @endif
                        </div>
                    @endif
                </div>
            </div>
        </div>
    </section>

    @include('site.includes.footer')
@endsection

@push('js')
    <script src="/vendor/tinymce/tinymce.min.js"></script>
    @if(auth()->check() && $question->user_id == auth()->user()->id && $question->service_id != 3)
        <script>
            //if question egasi bolsa
                $('.clp_q').on('click',function() {
                    var id = $(this).data('id');
                    tinymce.init({
                        selector: "#reply_add" + id,  // change this value according to your HTML
                        menubar:false,
                        statusbar: false,
                        plugins: "lists link paste ",
                        toolbar: 'undo redo|  bold italic | alignleft | bullist numlist | blockquote | link | paste',
                        width: '100%',
                        height: 300,
                    });
                });

                $('.btn_refusal_q').on('click',function() {
                    var id = $(this).data('id');
                    hideCollaple(id)
                })

                function hideCollaple(id)
                {
                    $('#collapseadded').collapse('hide')
                    //tinymce.remove("#reply_add" + id);
                }

            var form = document.forms.additional_form;
            form.addEventListener('submit',function(e){
                tinyMCE.triggerSave()
                e.preventDefault();
                var fd = new FormData(document.forms.additional_form);

                //console.log($('#reply_text53').val())
                var xhr = new XMLHttpRequest();
                xhr.open("POST", "{{ Route('questions.additional', [ $question->id ]) }}");
                xhr.responseType = 'json';

                xhr.send(fd);//FORM JUNATAMIZ

                xhr.ontimeout = function (e) {

                };

                xhr.onload  = function() {
                    var res = xhr.response;
                    if(xhr.readyState == 4) {
                        if(res.status == false) {

                            $('#alert_div').show();
                            $('#reply_alert').html('');
                            $.each(res.errors.full_text, function(index, key) {
                                $('#reply_alert').append("<li>"+ key +"</li>");
                            });

                            $.each(res.errors.files, function(index, key) {
                                $('#reply_alert').append("<li>"+ key +"</li>");
                            });


                        } else if(res.status == true) {
                            hideCollaple(res.id)
                            $('#additional_show').append(res.view);
                            tinyMCE.activeEditor.setContent('');
                        }
                    }else {

                    }
                };

                if (xhr.status != 200) {

                } else {

                }
            }, false);
            //end if questionni egasi bolsa
        </script>

        <script src="{{ mix('js/custom-file-input.js') }}"></script>
    @endif


    @if(auth()->check() && $question->service_id == 3 && auth()->user()->isLegalCounsel() ||auth()->check() && auth()->user()->isReply())
    <script>


        $('.reply').on('click',function(e) {
            e.preventDefault;
            var id = $(this).data('id');
            $(this).hide();

            $( '#reply_div'+id ).slideDown( "slow", function() {
                $(this).show();
            });

            //console.log(id);

            tinymce.init({
                selector: "#reply_text" + id,  // change this value according to your HTML
                menubar:false,
                statusbar: false,
                plugins: "lists link paste ",
                toolbar: 'undo redo|  bold italic | alignleft | bullist numlist | blockquote | link | paste',
                width: '100%',
                height: 300,
            });
        })


        $('.btn_refusal').on('click', function(e) {
            e.preventDefault;
            var id = $(this).data('id');

            hideReply(id, 1);

        })

        function hideReply(id, type)
        {
            $( "#reply_div"+id ).slideUp( "slow", function() {
                $(this).hide()
                tinymce.remove("#reply_text" + id);
                if(type == 1){
                    $('#reply'+id).show();
                }else {
                    //$('#success_alert_div').show();
                    $('#reply'+id).show();
                }


            });
        }



            var form = document.forms.reply_form;
            form.addEventListener('submit',function(e){
                tinyMCE.triggerSave()
                e.preventDefault();
                var fd = new FormData(document.forms.reply_form);

                //console.log($('#reply_text53').val())
                var xhr = new XMLHttpRequest();
                xhr.open("POST", "{{ Route('questions.answer-in', [ $question->id ]) }}");
                xhr.responseType = 'json';

                xhr.send(fd);//FORM JUNATAMIZ

                xhr.ontimeout = function (e) {
                    alert(3)
                };

                xhr.onload  = function() {
                    var res = xhr.response;
                    if(xhr.readyState == 4) {
                        if(res.status == false) {

                            $('#alert_div').show();
                            $('#reply_alert').html('');
                            $.each(res.errors.full_text, function(index, key) {
                                $('#reply_alert').append("<li>"+ key +"</li>");
                            });

                            $.each(res.errors.files, function(index, key) {
                                $('#reply_alert').append("<li>"+ key +"</li>");
                            });


                        } else if(res.status == true) {
                            hideReply(res.id,2)
                            $('#success_answered').text(res.message);
                            $('#answers_append').append(res.view)

                            tinyMCE.activeEditor.setContent('');

                            setTimeout(function() {
                                $('html, body').animate({
                                    scrollTop: $(".aanswer" + res.answer_id).offset().top
                                }, 2000);
                            }, 2000);
                        }
                    }else {

                    }
                };

                if (xhr.status == 200) {

                } else {

                }
            }, false);


    </script>

    <script src="{{ mix('js/custom-file-input.js') }}"></script>
    @endif

    @auth
        <script>

            $('.gratitude_btn').on('click',function(e){
                e.preventDefault();
                if ($(this).hasClass("gratitude-pressed")) {

                }else{
                    $(this).addClass('gratitude-pressed');
                    var id = $(this).data('id');
                    $.ajax({
                        url: "/lawyers/gratitude/" + id,
                        method: "POST",
                        data: {_token: "{{csrf_token()}}", id: id}
                    }).done(function(data) {
                        $('#gratitude-count'+id).text(data.count);
                        console.log(1)
                    });
                }
            });

            $('.btn_replya').on('click',function(e){
                e.preventDefault();
                var id = $(this).data('id');
                //console.log(id);
                var type = $(this).data('type');

                if($(this).hasClass('reply_btn')){
                    hideReply2(id,type)

                }else{
                    showReply(id,type)
                }
            })

            function showReply(id,type){
                if(type == 'answer'){
                    $('#abtn_reply' + id).addClass('reply_btn');
                    $( '#txta'+id ).slideDown( "slow", function() {
                        $(this).show();
                    });

                    tinymce.init({
                        selector: "#atextarea" + id,  // change this value according to your HTML
                        menubar:false,
                        statusbar: false,
                        plugins: "lists link paste ",
                        toolbar: 'undo redo|  bold italic | alignleft | bullist numlist | blockquote | link | paste',
                        width: '100%',
                        height: 300,
                    });
                }else if(type == 'sub_answer'){
                    $('#sbtn_reply' + id).addClass('reply_btn');
                    $( '#subtxta'+id ).slideDown( "slow", function() {
                        $(this).show();

                    });

                    // tinymce.init({
                    //     selector: "#stextarea" + id,  // change this value according to your HTML
                    //     width: '100%',
                    //     height: 300,
                    // });
                }
            }

            function hideReply2(id,type){
                if(type == 'answer'){
                    $( "#txta"+id ).slideUp( "slow", function() {
                        $(this).hide()
                        tinymce.remove("#atextarea" + id);
                    });
                    $('#abtn_reply'+id).removeClass("reply_btn")
                }else if(type == 'sub_answer'){
                    $('#sbtn_reply'+id).removeClass("reply_btn")
                    $( "#subtxta"+id ).slideUp( "slow", function() {
                        $(this).hide()
                        tinymce.remove("#stextarea" + id);
                    });
                }
            }

            $('.btn-close').on('click',function(e){
                e.preventDefault();
                var id = $(this).data('id');
                var type = $(this).data('type');
                hideReply2(id,type)
            })

            $('.btn-answer-reply').on('click',function(e){
                e.preventDefault();
                var id = $(this).data('id');
                var type = $(this).data('type');
                //var text = tinymce.activeEditor.getContent({format: 'raw'});
                sendReply(id,type);
            })

            function sendReply(id,type){
                if(type == 'answer'){
                    var url = "/questions/additional/answer/" + id;
                    sendReplyAjax(id,type,url)
                }else if(type == 'sub_answer'){
                    var url = "/questions/additional/subrequest/ajax/" + id;
                    sendReplyAjax(id,type,url)
                }
            }


            function sendReplyAjax(id,type,url)
            {
                var text = tinymce.activeEditor.getContent({format: 'raw'});
                $.ajax({
                    url: url,
                    method: "POST",
                    data: {_token: "{{csrf_token()}}", id: id, text:text}
                }).done(function(data) {
                    if(data.status == false) {
                        $('#alert_div' + data.id).show();
                        $('#reply_alert'+ data.id).html('');
                        $.each(data.errors.full_text, function(index, key) {
                            $('#reply_alert'+ data.id).append("<li>"+ key +"</li>");
                        });
                    } else if (data.status == true){
                        hideReply2(id,type);
                        $('#subanswers'+id).append(data.view);

                    }


                });
            }
        </script>
    @endauth

    <script type="text/javascript" src="//s7.addthis.com/js/300/addthis_widget.js#pubid=ra-57237a7575292f04"></script>

@endpush


@push('meta')
    <meta name="description" content="{{ Str::limit(strip_tags($question->full_text),250)}}">
    <meta name="keywords" content="{{str_replace(' ',',',$question->title)}} {{str_replace(' ',',', Str::limit(strip_tags($question->full_text), 250))}}">
    <meta name="og:title" property="og:title" content="{{ $question->title }}">
    <meta property="og:description" content="{{ Str::limit(strip_tags($question->full_text),250) }}">
    <meta property="og:type" content="article">
    <meta property="og:url" content="{{ route('questions.view',[ $question->id, $question->slug ]) }}">
    <meta property="og:site_name" content="Yuridik.uz">
@endpush
