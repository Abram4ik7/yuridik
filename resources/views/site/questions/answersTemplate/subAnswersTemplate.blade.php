@foreach($subAnswers as $answer)
    <div class="mycard-inner my-card-inner_sub_div">
        <div class="shadow my-card-no-radius p-2 card  shadow zindex-100">
            <div class="col-md-12">
                <div class="col-md-3 col-lg-3 col-xl-3 rounded-circle float-left">
                    <center>
                        <div class=" d-inline-block">
                            @if(!empty($answer->users->photo) and is_file($answer->users->photo))
                                <img class=" rounded-circle" height="120px" width="120px"  src="/{{ $answer->users->photo }}" />
                            @else
                                <img class=" rounded-circle" height="120px" width="120px"  src="/images/noavatar.png" />
                            @endif
                            <span class="text-left">
                                <h6 class="mt-1 d-block margin-top-10" style="font-size:14px; margin-bottom: 0px;">
                                    <center>
                                        <i class="fa fa-user"></i>
                                        @if(!empty($answer->users->last_name))
                                            {{ $answer->users->last_name }}
                                        @endif
                                        @if(!empty($answer->users->first_name))
                                            {{ $answer->users->first_name }}
                                        @endif
                                    </center>
                                </h6>

                                <small class="d-block">
                                    <center>
                                        <i class="fa fa-map-marker"></i> {{ $answer->users->city->name() }}
                                    </center>
                                </small>
                                <small class="d-flex pt-1 pb-1 pr-2 pl-2">
                                    <span  id='user-gratitude-{{ $answer->id }}' class="float-left" style="color:green"><i class="fa fa-thumbs-up"></i> <span id="gratitude-count{{$answer->user_id}}">@if(!empty($answer->users->thanks)){{  $answer->users->thanks }} @else 0 @endif</span></span>&nbsp;&nbsp;&nbsp;
                                    <span class="float-rigth">@lang('site.questions.gratitude')</span>
                                </small>
                            </span>
                        </div>
                    </center>
                </div>
                <div class="col-md-9 col-lg-9 col-xl-9 float-left">
                    {!! $answer->full_text !!}


                    @if($answer->files)
                        @foreach($answer->files as $file)
                            <div class="col-md-12 float-left ">
                                <div class="row">
                                    <a href="{{ Route('download_file', [ $file->id ]) }}" class="col-md-8 padding0">
                                        <div class="col-md-12 download-file shadow  float-left margin-top-10">
                                            <div class="row">
                                                <div class="col-md-2 float-left">
                                                    <svg class="svg-icon svg-icon--svg_icon_staple"><use xmlns:xlink="http://www.w3.org/1999/xlink" xlink:href="/assets/extension/icons.svg#svg-icon-staple"></use></svg>
                                                </div>

                                                <div class="col-md-8 download-file-name float-left padding0">
                                                        <span class="color_blue">
                                                            {{ $file->substr() }}
                                                        </span>
                                                    <span class="float-right">
                                                            {{round($file->filesize / 1024)}} kbayt
                                                        </span>
                                                </div>

                                                <div class="col-md-2 float-left">
                                                    <svg class="svg-icon svg-icon--download"><use xmlns:xlink="http://www.w3.org/1999/xlink" xlink:href="/assets/extension/icons.svg#svg-icon-download"></use></svg>
                                                </div>
                                            </div>
                                        </div>
                                    </a>
                                </div>
                            </div>
                        @endforeach
                    @endif



                    <div class="card-body d-flex anwers_footer_padding">
                        @if(Auth::check() && Auth::user()->id != $answer->user_id)
                            <a href="" class="mr-3 d-flex border text-cplt border-green   rounded pt-1 pb-1 pr-2 pl-2 gratitude_btn @if(Session::get('gratitude'.$answer->user_id)) gratitude-pressed @endif" id="gratitude_btn{{$answer->user_id}}" data-id="{{$answer->user_id}}">
                                <span id="user-gratitude-5" class="d-flex float-left" style="color:green;"><i class="pt-1 pr-1 fa fa-thumbs-up"></i> </span>&nbsp;
                                <span  class="float-rigth text-green">@lang('site.questions.gratitude')</span>
                            </a>
                        @endif
                        @auth
                            <a href="" class="d-flex border rounded pt-1 pb-1 pr-2 pl-2 btn_replya" id="abtn_reply{{$answer->id}}" data-id="{{$answer->id}}" data-type="answer">
                                    <span id="user-gratitude-5" class="d-flex float-left" style="color: #0039b5;">
                                        <i class="pt-1 pr-1 fa fa-mail-reply-all"></i>
                                    </span>&nbsp;

                                <span  class="float-rigth text-myblue">@lang('site.questions.reply_answer')</span>
                            </a>
                        @endauth


                    </div>
                    @auth
                        <div class="w100" id="txta{{$answer->id}}" style="display: none">
                            <div class="alert alert-danger" id="alert_div{{$answer->id}}" style="display: none;">
                                <i class="fa fa-info-circle"></i> @lang('site.information')<br>
                                <div id="reply_alert{{$answer->id}}">

                                </div>
                            </div>
                            <textarea id="atextarea{{$answer->id}}"></textarea>
                            <div class="text-right margin-top-10">
                                <button class="btn btn-default btn-close" data-id="{{$answer->id}}" data-type="answer"><i class="fa fa-remove"></i> @lang('site.questions.otmen')</button>
                                <button class="btn btn-success btn-answer-reply"  data-id="{{$answer->id}}" data-type="answer" ><i class="fa fa-comment"></i> @lang('site.questions.answer_reply')</button>
                            </div>
                        </div>
                    @endauth
                </div>
            </div>
        </div>


        <div id="subanswers{{$answer->id}}">
            @if(count($answer->ss_answers) > 0)
                @include('site.questions.answersTemplate.subAnswersTemplate', ['subAnswers' => $answer->ss_answers])
            @endif
        </div>
    </div>
@endforeach
