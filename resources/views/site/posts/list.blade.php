@extends('site.layouts.app')
@section('title', trans('site.posts').' - ')

@section('content')
    <section class="slice slice-lg bg-gradient-primary">
        <a href="#sct-article" class="tongue tongue-bottom tongue-white scroll-me">
            <i class="fa fa-angle-down"></i>
        </a>
    </section>

    <section class="slice  bg_first" id="sct-article">
        <div class="container">
            <div class="text-center mb-5">
                <h1 class="margin-bottom-40 strong-600 color_blue">
                    <center>
                        @lang('site.posts')
                    </center>
                </h1>
            </div>
        </div> <!-- container -->

        <div class="container">
            @if(count($posts) == 0)
                <div class="alert alert-info alert-dismissible fade show" role="alert">
                    <span class="alert-inner--icon"><i class="fas fa-info"></i></span>
                    <span class="alert-inner--text"><strong>@lang('site.information')</strong> @lang('site.not_posts')</span>
                    <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
            @endif

            <div class="row justify-content-center">
                @php
                    $c = 1;
                @endphp
                @foreach($posts as $post)
                    @if($c++ % 3 == 1)
                        <div class="justify-content-center row">
                    @endif
                        <div class="col-md-4">
                            <div class="maqolalar_item mywidth mb-3 shadow">
                                @if($post->poster && is_file($post->poster))
                                    <div class="top-img" style="background: url(/{{ $post->poster }});"></div>
                                @else
                                    <div class="top-img" style="background: url(/assets/img/nophoto.jpg);"></div>
                                @endif
                                <span class="mql_title">
                                           <b>{{$post->title}}</b>
                               </span>
                                <hr>
                                <div class="mql_details">
                                    <i class="fa fa-calendar"></i> {{ date('d.m.Y, H:i', strtotime($post->updated_at)) }}

                                    <span class="float-right text-right"> <i class="fa fa-eye"></i> {{$post->views}}</span>
                                </div>
                                <hr>

                                <p>{{ Str::limit(mb_substr(strip_tags($post->full_text),100), 80) }}</p>
                                <a href="{{ Route('posts.view', [ $post->id, $post->slug ]) }}" class="mql_a">@lang('site.read_more')</a>
                            </div>
                        </div>

                            @if($c % 3 == 1)
                                </div>
                            @endif
                @endforeach
                    <div class="col-md-12">
                        {{$posts->links()}}
                    </div>
            </div> <!-- row -->

        </div> <!-- container -->
    </section>
    @include('site.includes.footer')
@endsection

@push('css')
    <style>
        .pagination{
            justify-content: center;
        }
    </style>
@endpush