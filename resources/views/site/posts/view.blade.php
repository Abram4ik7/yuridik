@extends('site.layouts.app')
@section('title', $data->title.' - ')

@section('content')
    <section class="slice slice-lg bg-gradient-primary">
        <a href="#sct-article" class="tongue tongue-bottom tongue-white scroll-me">
            <i class="fa fa-angle-down"></i>
        </a>
    </section>

    <section class="slice  bg_first w-100 float-left" id="sct-article">
        <div class="container">
                <div class="col-lg-9 float-left">
                    <div class="card">
                        <div class="card-body p-5">
                            <!-- Topic header -->
                            <h2 class="h2 mb-2">{{ $data->title }}</h2>
                            <div class="media align-items-center mt-4">
                                <a href="#" class="avatar avatar-sm rounded-circle mr-3">
                                    @if($data->user->photo && is_file($data->user->photo))
                                        <img alt="{{ $data->user->first_name }} {{ $data->user->last_name }}" src="/{{ $data->user->photo }}">
                                    @else
                                        <img alt="{{ $data->user->first_name }} {{ $data->user->last_name }}" src="/images/noavatar.png">
                                    @endif
                                </a>
                                <div class="media-body">
                                    <span class="h6 mb-0">{{ $data->user->first_name }} {{ $data->user->last_name }}</span>
                                    <span class="text-sm text-muted">
                                        {{ date('d.m.Y, H:i', strtotime($data->updated_at)) }}
                                    </span>
                                </div>
                            </div>
                            <div class="d-flex align-items-center mt-4">
                                <ul class="list-inline">
                                    <li class="list-inline-item pr-3">
                                        <span class="badge badge-lg badge-pill badge-info">
                                            {{ $data->speciality->name() }}
                                        </span>
                                    </li>
                                    <li class="text-sm list-inline-item pr-3"><i class="fas fa-eye mr-2"></i> {{ $data->views }}</li>
                                </ul>
                            </div>

                            <!-- Topic body -->
                            <article class="mt-5 full_text_posts">
                                @if($data->poster  && is_file($data->poster))
                                    <center>
                                        <img src="/{{ $data->poster }}" class="margin-bottom-20" alt="{{ $data->title }}">
                                    </center>
                                @endif

                                {!! $data->full_text !!}
                            </article>
                        </div>

                    </div>
                </div>{{-- content--}}


            <div class="col-lg-3 float-left">
                @foreach($posts as $post)
                    <div class="col-md-12">
                        <div class="row">
                            <div class="maqolalar_item w-100 mywidth mb-3 shadow">
                                @if($post->poster && is_file($post->poster))
                                    <div class="top-img" style="background: url(/{{ $post->poster }});"></div>
                                @else
                                    <div class="top-img" style="background: url(/assets/img/nophoto.jpg);"></div>
                                @endif
                                <span class="mql_title">
                                           {{$post->title}}
                               </span>
                                <hr>
                                <div class="mql_details">
                                    <i class="fa fa-calendar"></i> {{ date('d.m.Y, H:i', strtotime($post->updated_at)) }}

                                    <span class="float-right text-right"> <i class="fa fa-eye"></i> {{$post->views}}</span>
                                </div>
                                <hr>

                                <a href="{{ Route('posts.view', [ $post->id, $post->slug ]) }}" class="mql_a">@lang('site.read_more')</a>
                            </div>
                        </div>
                    </div>
                @endforeach
            </div>{{-- relates --}}
        </div> <!-- container -->
    </section>
    @include('site.includes.footer')
@endsection

@push('meta')
    <meta name="description" content="{{ Str::limit(strip_tags($post->full_text), 250) }}">
    <meta name="keywords" content="{{ str_replace(' ',',',$post->title) }} {{ str_replace(' ',',',strip_tags(Str::limit($post->full_text, 250))) }}">
    <meta name="og:title" property="og:title" content="{{ $post->title }}">
    <meta property="og:description" content="{{ Str::limit(strip_tags($post->full_text), 250) }}">
    <meta property="og:type" content="article">
    <meta property="og:url" content="{{ Route('posts.view',[ $post->id, $post->slug ]) }}">
    <meta property="og:site_name" content="Yuridik.uz">
    @if($post->poster)
        <meta property="og:image" content="https://yuridik.uz/{{ $post->poster }}">
    @endif
@endpush