<section class="slice slice-lg w-100 float-left" id="other" style="padding-top: 30px">
    <div class="container" style="margin-bottom: 50px">
        <div class="row">
            <div class="col-md-12">
                <h1 class=" strong-600 color_blue pt-5">
                    <center>

                        Boshqa xizmatlar
                    </center>
                </h1>
            </div>
            <div class="col-md-12">
                <center>
                    <div class="col-md-6">
                        <p style="font-size: 20px">
                            Saytda savol berishingiz mumkin, bundan tashqari telefon orqali konsultatsiya olishingiz
                            hamda kerakli hujjatlarni buyurtma qilishingiz mumkin</p>
                    </div>
                </center>
            </div>
        </div>
    </div>
    <br>
    <br>
    <div class="container">
        <div class="row">
            <div class="col-md-6 mb-5 mb-md-0">
                <div class="card myeffect myborder bg-gradient-secondary shadow shadow-lg--hover border-0 position-relative zindex-100">
                    <div class="card-body py-5">
                        <div class="myprice">
                            12 000 so'mdan
                        </div>
                        <a href="#" class="mybtn3">Foydalanish</a>
                        <div class="d-flex align-items-start">
                            <div class="icon main-color">
                                <i class="fas fa-phone"></i>
                            </div>
                            <div class="icon-text">
                                <h3 class="h4">Telefon konsultatsiya</h3>
                                <p class="mb-0">Telefon raqamizgizni qoldiring va mutaxasis siz bilan tez orada
                                    bog'lanadi.
                                </p>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="col-md-6 mb-5 mb-md-0">
                <div class="card myeffect myborder bg-gradient-secondary shadow shadow-lg--hover border-0 position-relative zindex-100">


                    <div class="card-body py-5">
                        <div class="myprice">
                            10 000 so'mdan
                        </div>
                        <a href="#" class="mybtn3">Foydalanish</a>
                        <div class="d-flex align-items-start">
                            <div class="icon main-color">
                                <i class="fas fa-question-circle"></i>
                            </div>
                            <div class="icon-text">
                                <h3 class="h4">Telefon konsultatsiya</h3>
                                <p class="mb-0">Telefon raqamizgizni qoldiring va mutaxasis siz bilan tez orada
                                    bog'lanadi.
                                </p>
                            </div>
                        </div>
                    </div>


                </div>
            </div>
        </div>
    </div>
</section>