<!DOCTYPE html>
<html lang="{{ App::getLocale() }}" class="translated-ltr">
<head>
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <meta name="description" content="">

    <!-- CSRF Token -->
    <meta name="csrf-token" content="{{ csrf_token() }}">

    <title>@yield('title'){{Setting::find(1)->name()}}</title>

    <!-- Favicon -->
    <link rel="icon" href="/assets/images/favicon.png" type="image/png">
    <link type="text/css" rel="stylesheet" charset="UTF-8" href="{{ mix('assets/css/all.css') }}">
    @stack('css')
    @stack('meta')
</head>

<body>

<div id="yuridik">
<header class="header-transparent" id="header-main">
    <!-- Search -->
    <div id="search-main" class="navbar-search">
        <div class="container">
            <!-- Search form -->
            <form class="navbar-search-form" role="form">
                <div class="form-group">
                    <div class="input-group input-group-transparent">
                        <div class="input-group-prepend">
                            <span class="input-group-text"><i class="fa fa-search"></i></span>
                        </div>
                        <input type="text" class="form-control" placeholder="Type and hit enter ...">
                    </div>
                </div>
            </form>
            <div class="navbar-search-suggestions">
                <h6>Поиск предложений</h6>
                <div class="row">
                    <div class="col-sm-6">
                        <ul class="list-unstyled mb-0">
                            <li>
                                <a class="list-link" href="https://preview.webpixels.io/purpose-website-ui-kit-v1.1.3/#">
                                    <i class="far fa-search"></i>
                                    <span>macbook pro</span> в ноутбуках
                                </a>
                            </li>
                            <li>
                                <a class="list-link" href="https://preview.webpixels.io/purpose-website-ui-kit-v1.1.3/#">
                                    <i class="far fa-search"></i>
                                    <span>iphone 8</span> в смартфонах
                                </a>
                            </li>
                            <li>
                                <a class="list-link" href="https://preview.webpixels.io/purpose-website-ui-kit-v1.1.3/#">
                                    <i class="far fa-search"></i>
                                    <span>macbook pro</span> в ноутбуках
                                </a>
                            </li>
                            <li>
                                <a class="list-link" href="https://preview.webpixels.io/purpose-website-ui-kit-v1.1.3/#">
                                    <i class="far fa-search"></i>
                                    <span>бьет про соло 3</span> в наушниках
                                </a>
                            </li>
                            <li>
                                <a class="list-link" href="https://preview.webpixels.io/purpose-website-ui-kit-v1.1.3/#">
                                    <i class="far fa-search"></i>
                                    <span>smasung galaxy 10</span> в телефонах
                                </a>
                            </li>
                        </ul>
                    </div>
                </div>
            </div>
        </div>
    </div><!--search-->
    <!-- Topbar -->
    <div id="navbar-top-main" class="navbar-top  navbar-dark bg-dark border-bottom">
        <div class="container">
            <div class="navbar-nav align-items-center">
                <div>
                    <ul class="nav">
                        <li class="nav-item dropdown ml-lg-2 dropdown-animate" data-toggle="hover">
                            <a class="nav-link px-0" href="#" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                                <img alt="Заполнитель изображений" src="/assets/img/elem10.png">
                                <span class="d-none d-lg-inline-block">Русский язык</span>
                                <span class="d-lg-none">RU</span>
                            </a>
                            <div class="dropdown-menu dropdown-menu-sm dropdown-menu-arrow">
                                <a href="#" class="dropdown-item">
                                    <img alt="Заполнитель изображений" src="/assets/images/es.svg">O`zbek tili </a>

                            </div>
                        </li>
                    </ul>
                </div>
                <div class="ml-auto">
                    <ul class="nav">
                        <li class="nav-item">
                            <a class="nav-link" href="/feedback">Yordam</a>
                        </li>
                        <li class="nav-item">
                            <a href="https://preview.webpixels.io/purpose-website-ui-kit-v1.1.3/#" class="nav-link" data-action="search-open" data-target="#search-main"><i class="fa fa-search"></i></a>
                        </li>
                        <li class="nav-item">
                            <a class="nav-link" href="/login"><i class="far fa fa-user-circle"></i></a>
                        </li>
                    </ul>
                </div>
            </div>
        </div>
    </div>

    <!-- Main navbar -->

    <nav class="navbar navbar-main navbar-expand-lg navbar-sticky navbar-transparent navbar-dark bg-dark" id="navbar-main">
        <div class="container">
            <a class="navbar-brand mr-lg-5" href="#">
                <img alt="Заполнитель изображений" src="/assets/img/logo.png" style="height: 50px;">
            </a>

            <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbar-main-collapse" aria-controls="navbar-main-collapse" aria-expanded="false" aria-label="Toggle navigation">
                <span class="navbar-toggler-icon"></span>
            </button>

            <div class="collapse navbar-collapse" id="navbar-main-collapse">
                <ul class="navbar-nav align-items-lg-center">
                    <li class="nav-item {{ active(route('lawyers')) }}">
                        <a class="nav-link" href="{{ route('lawyers') }}">
                            @lang('site.lawyer')
                        </a>
                    </li>
                    <li class="nav-item {{ active(route('posts')) }}" >
                        <a class="nav-link" href="{{ route('posts') }}" >
                            @lang('site.posts')
                        </a>

                    </li>
                    <li class="nav-item {{ active(route('questions')) }}">
                        <a class="nav-link" href="{{ Route('questions') }}">
                            @lang('site.questions.title')
                        </a>
                    </li>

                    <li class="nav-item dropdown dropdown-animate"  data-toggle="hover">
                        <a class="nav-link" href="#" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false" role="button">Telefon konsultatsiya</a>
                        <div class="dropdown-menu dropdown-menu-lg dropdown-menu-arrow py-0">
                            <div class="list-group">
                                <a href="https://preview.webpixels.io/purpose-website-ui-kit-v1.1.3/pages/pages-landing.html" class="list-group-item list-group-item-action" >
                                    <div class="media d-flex align-items-center">
                                        <i class="fa fa-phone"></i>
                                        <div class="media-body ml-3">
                                            <h6 class="mb-1">Konsultatsiya</h6>
                                        </div>
                                    </div>
                                </a>
                                <a href="https://preview.webpixels.io/purpose-website-ui-kit-v1.1.3/pages/pages-secondary.html" class="list-group-item list-group-item-action">
                                    <div class="media d-flex align-items-center">
                                        <i class="fa fa-plus-circle"></i>
                                        <div class="media-body ml-3">
                                            <h6 class="mb-1">Buyurtma berish</h6>
                                        </div>
                                    </div>
                                </a>

                            </div>
                        </div>
                    </li>
                </ul>

                <ul class="navbar-nav align-items-lg-center ml-lg-auto">

                    <li class="nav-item">
                        <a class="nav-link" href="#" target="_blank">Bu qanday ishlaydi?</a>
                    </li>
                    <li class="nav-item mr-0">
                        <a href="#" target="_blank" class="nav-link d-lg-none">Savol bering</a>
                        <a href="#" target="_blank" class="btn btn-sm btn-white btn-circle btn-icon d-none d-lg-inline-flex">
                            <span class="btn-inner--icon"><i class="far fa fa-question"></i></span>
                            <span class="btn-inner--text">Savol bering</span>
                        </a>
                    </li>
                </ul>

            </div>
        </div>
    </nav>

</header>



<main>
    @yield('content')
</main>



<footer class="footer footer-dark bg-gradient-primary footer-rotate w-100 float-left">
    <div class="container">
        <div class="row pt-md">
            <div class="col-lg-5 mb-5 mb-lg-0">
                <a href="#">
                    <img src="/assets/img/logo.png" class="mb-2" alt="Нижний колонтитул" style="height: 50px;">
                </a>
                <p class="text-sm">Yuridik.uz - huquqshunoslar va mijozlar o'rtasidagi aloqa vositasini bajaruvchi, onlayn yuridik xizmat ko'rsatish platformasi. Xizmat sifati sizni qanoatlantirmasa pulingizni qaytaramiz.</p>
            </div>
            <div class="col-lg-3 col-6 col-sm-4 ml-lg-auto mb-5 mb-lg-0">
                <h6 class="heading mb-3">Kompaniya</h6>
                <ul class="list-unstyled">
                    <li><a href="about.html">Loyiha haqida</a>
                    </li>
                    <li><a href="how-works.html">Bu qanday ishlaydi?</a>
                    </li>
                    <li><a href="#">Foydalanish shartlari</a>
                    </li>
                    <li><a href="faq.html">Ko'p beriladigan savollar</a>
                    </li>
                </ul>
            </div>
            <div class="col-lg-3 col-6 col-sm-4 mb-5 mb-lg-0">
                <h6 class="heading mb-3">Boshqalar</h6>
                <ul class="list-unstyled text-small">
                    <li><a href="https://preview.webpixels.io/purpose-website-ui-kit-v1.1.3/#">Fikr qoldirish</a>
                    </li>
                    <li><a href="contact.html">Contact</a>
                    </li>
                    <li><a href="https://preview.webpixels.io/purpose-website-ui-kit-v1.1.3/#">Karyera</a>
                    </li>
                </ul>
            </div>

        </div>
        <div class="row align-items-center justify-content-md-between py-4 mt-4 delimiter-top">
            <div class="col-md-6">
                <div class="copyright text-sm font-weight-bold text-center text-md-left">
                    © Yuridik.uz {{ date('Y',time()) }} <a href="https://usoft.uz/" class="font-weight-bold" target="_blank">Umbrella Soft</a> . Barcha huquqlar himoyalangan
                </div>
            </div>
            <div class="col-md-6">
                <ul class="nav justify-content-center justify-content-md-end mt-3 mt-md-0">
                    <li class="nav-item">
                        <a class="nav-link" href="https://dribbble.com/" target="_blank">
                            <i class="fab fa-dribbble"></i>
                        </a>
                    </li>
                    <li class="nav-item">
                        <a class="nav-link active" href="https://instagram.com/" target="_blank">
                            <i class="fab fa-instagram"></i>
                        </a>
                    </li>
                    <li class="nav-item">
                        <a class="nav-link" href="https://github.com/" target="_blank">
                            <i class="fab fa-github"></i>
                        </a>
                    </li>
                    <li class="nav-item">
                        <a class="nav-link" href="https://facebook.com/" target="_blank">
                            <i class="fab fa-facebook"></i>
                        </a>
                    </li>
                </ul>
            </div>
        </div>
    </div>
</footer>

<div id="overlayBase" >
    <div class="loader-wrap center-block">
        <img src="/assets/img/preloader.png" width="250">
    </div>
    <div class="loader-review">
        <div class="loading center">
            <div class="loading-bar"></div>
        </div>
    </div>
</div>

</div>


<!-- Core -->

<script type="text/javascript" src="{{ mix('js/all.js') }}"></script>
@stack('js')

</body>
</html>