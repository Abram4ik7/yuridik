@extends('site.layouts.app')
@section('title', trans('site.lawyers').' - ')
@section('content')
    <section class="slice slice-lg bg-gradient-primary">
        <div class="container pt-lg">
            <div class="row justify-content-center">
                <div class="col-lg-9">
                    <form>
                        <div class="form-group bg-white rounded px-2 py-2 shadow">
                            <div class="row myclass1">
                                <div class="col-md-8">
                                    <div class="input-group input-group-transparent shadow-none">
                                        <div class="input-group-prepend">
                                            <span class="input-group-text border-0"><i class="fa fa-search"></i></span>
                                        </div>
                                        <input type="text" class="form-control border-0 shadow-none"
                                               placeholder="@lang('site.search')">
                                    </div>
                                </div>
                                <div class="col-md-4 text-right">
                                    <button type="button" class="btn btn-primary btn-icon">
                                        <span class="btn-inner--icon"><i class="fa fa-search"></i></span>
                                        <span class="btn-inner--text">@lang('site.search')</span>
                                    </button>
                                </div>
                            </div>
                        </div>
                    </form>
                </div>
            </div>
        </div>
        <a href="#sct-article" class="tongue tongue-bottom tongue-white scroll-me">
            <i class="fa fa-angle-down"></i>
        </a>
    </section>


    <section class="slice" id="sct-article">
        <div class="container">
            <div class="text-center mb-5">
                <h1 class=" margin-bottom-40 strong-600 color_blue">
                    <center>
                        @lang('site.lawyers')
                    </center>
                </h1>
            </div>
            <div class="row">
                <div class="col-lg-8">
                    <div class="row">
                        @foreach($lawyers as $lawyer)
                            <div class="col-md-4 col-xs-6">
                                <a href="{{ Route('users-view',[ $lawyer->id, str_slug($lawyer->last_name) ]) }}"
                                   class="w-100 h-100">
                                    <div class="maqolalar_item mb-3 p-4 pb-5 shadow">

                                        <div class="text-center">
                                            @if($lawyer->photo && is_file($lawyer->photo))
                                                <img class="rounded-circle myimg1" src="/{{ $lawyer->photo }}"
                                                     alt="{{ $lawyer->first_name }} {{ $lawyer->last_name }}">
                                            @else
                                                <img class="rounded-circle myimg1" src="/images/noavatar.png"
                                                     alt="{{ $lawyer->first_name }} {{ $lawyer->last_name }}">
                                            @endif
                                        </div>
                                        <span class="mql_title">
                                                   <b>{{ $lawyer->first_name }} {{ $lawyer->last_name }}</b>
                                               </span>
                                        <p>{{ $lawyer->city->name() }}</p>
                                        <hr>
                                        <span class="float-left text-success">
                                                {{ $lawyer->thanks }}
                                        </span>
                                        <p class="float-left mb-0 mt-1 ml-1 text-success"> @lang('site.users.thanks')</p>
                                    </div>
                                </a>
                            </div>
                        @endforeach

                        {{ $lawyers->links() }}

                    </div>


                </div>
                <div class="col-lg-4">
                    <h3 class="text-center">
                        @lang('site.active_lawyers')
                    </h3>
                    @foreach($randLawyers as $lawyer)
                        <div class="col-md-12 mb-3 p-0">
                            <div class="mycard shadow ">
                                <a href="{{ Route('users-view',[ $lawyer->id, str_slug($lawyer->last_name) ]) }}" class="w-100 h-100 p-3 d-flex">
                                    @if($lawyer->photo && is_file($lawyer->photo))
                                        <img class="rounded-circle myimg2" src="/{{ $lawyer->photo }}"
                                             alt="{{ $lawyer->first_name }} {{ $lawyer->last_name }}">
                                    @else
                                        <img class="rounded-circle myimg2" src="/images/noavatar.png"
                                             alt="{{ $lawyer->first_name }} {{ $lawyer->last_name }}">
                                    @endif

                                    <div class="d-flex ml-3 flex-column">
                                         <span class="mql_title">
                                           <b>{{ $lawyer->first_name }} {{ $lawyer->last_name }}</b>
                                         </span>
                                        <p class="mb-0" style="font-size: 12px;">
                                            {{ $lawyer->city->name() }}
                                        </p>
                                        <hr>
                                        <div class="d-flex">
                                           <span class="text-success">
                                            {{ $lawyer->thanks }}
                                        </span>
                                            <p class="mb-0 mt-0 ml-1 text-success"> @lang('site.users.thanks')</p>
                                        </div>
                                    </div>
                                    <a href="{{ Route('users-view',[ $lawyer->id, str_slug($lawyer->last_name) ]) }}" class="mybtn2">@lang('site.more')</a>
                                </a>
                            </div>
                        </div>
                    @endforeach


                </div>
            </div>
            <div class="row pt-5">
                <div class="col-md-12 col-lg-12 col-xl-12">
                    <center>

                    </center>
                </div>
            </div>
        </div>
    </section>

    @include('site.includes.footer')
@endsection