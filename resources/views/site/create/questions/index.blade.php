@extends('site.layouts.app')

@section('content')
    <question-create></question-create>
@endsection

@push('js')
    <script src="{{ mix('js/manifest.js') }}"></script>
    <script src="{{ mix('js/vendor.js') }}"></script>
    <script src="{{ mix('js/app.js') }}"></script>

    <!-- Build Yuridik Instance -->
    <script>
        window.Yuridik = new CreateYuridik();
    </script>

    <!-- Start Yuridik -->
    <script>
        Yuridik.liftOff();
    </script>
@endpush
