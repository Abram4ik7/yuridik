<div class="sidebar-sticky">
    <ul class="nav flex-column">
        <li class="nav-item">
            <a class="nav-link{{ active('admin.dashboard') ? ' active' : '' }}" href="{{ route('admin.dashboard') }}">
                {{ trans('Dashboard') }}
            </a>
        </li>

        <li class="nav-item">
            <a class="nav-link{{ active('admin.users') ? ' active' : '' }}" href="{{ route('admin.users') }}">
                {{ trans('Users') }}
            </a>
        </li>

        <li class="nav-item">
            <a class="nav-link" href="#">
                Products
            </a>
        </li>

        <li class="nav-item">
            <a class="nav-link" href="#">
                Customers
            </a>
        </li>

        <li class="nav-item">
            <a class="nav-link" href="#">
                Reports
            </a>
        </li>

        <li class="nav-item">
            <a class="nav-link" href="#">
                Integrations
            </a>
        </li>
    </ul>

    <h6 class="sidebar-heading text-muted">
        Saved reports
    </h6>

    <ul class="nav flex-column mb-2">
        <li class="nav-item">
            <a class="nav-link" href="#">
                Current month
            </a>
        </li>

        <li class="nav-item">
            <a class="nav-link" href="#">
                Last quarter
            </a>
        </li>

        <li class="nav-item">
            <a class="nav-link" href="#">
                Social engagement
            </a>
        </li>

        <li class="nav-item">
            <a class="nav-link" href="#">
                Year-end sale
            </a>
        </li>
    </ul>
</div>
