@extends('admin.app')

@section('admin.content')
    <div class="d-flex justify-content-between flex-wrap flex-md-nowrap align-items-center pt-3 pb-2 mb-3 border-bottom">
        <h2 class="h2">
            {{ trans('Dashboard') }}
        </h2>
    </div>

    <div class="row">
        <div class="col-sm-12 col-md-6 col-xl-3">
            <div class="card border-primary mb-3">
                <blockquote class="blockquote card-body text-primary mb-0">
                    <h3 class="d-flex justify-content-between align-items-center">
                        <span>{{ trans('Users') }}</span>
                        <span class="d-flex align-items-center text-muted">{{ $users }}</span>
                    </h3>

                    <footer class="blockquote-footer">
                        <small class="text-muted">
                            {{ trans('All users') }}
                        </small>
                    </footer>
                </blockquote>
            </div>
        </div>

        <div class="col-sm-12 col-md-6 col-xl-3">
            <div class="card border-danger mb-3">
                <blockquote class="blockquote card-body text-danger mb-0">
                    <h3 class="d-flex justify-content-between align-items-center">
                        <span>{{ trans('Questions') }}</span>
                        <span class="d-flex align-items-center text-muted">0</span>
                    </h3>

                    <footer class="blockquote-footer">
                        <small class="text-muted">
                            {{ trans('All questions') }}
                        </small>
                    </footer>
                </blockquote>
            </div>
        </div>

        <div class="col-sm-12 col-md-6 col-xl-3">
            <div class="card border-secondary mb-3">
                <blockquote class="blockquote card-body text-secondary mb-0">
                    <h3 class="d-flex justify-content-between align-items-center">
                        <span>{{ trans('Posts') }}</span>
                        <span class="d-flex align-items-center text-muted">0</span>
                    </h3>

                    <footer class="blockquote-footer">
                        <small class="text-muted">
                            {{ trans('All posts') }}
                        </small>
                    </footer>
                </blockquote>
            </div>
        </div>

        <div class="col-sm-12 col-md-6 col-xl-3">
            <div class="card border-info mb-3">
                <blockquote class="blockquote card-body text-info mb-0">
                    <h3 class="d-flex justify-content-between align-items-center">
                        <span>{{ trans('Payments') }}</span>
                        <span class="d-flex align-items-center text-muted">0</span>
                    </h3>

                    <footer class="blockquote-footer">
                        <small class="text-muted">
                            {{ trans('All payments') }}
                        </small>
                    </footer>
                </blockquote>
            </div>
        </div>
    </div>
@endsection
