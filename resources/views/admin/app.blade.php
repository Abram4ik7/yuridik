<!DOCTYPE html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">

    <!-- CSRF Token -->
    <meta name="csrf-token" content="{{ csrf_token() }}">

    <!-- Title -->
    <title>{{ config('app.name') }}</title>

    <!-- Fonts -->
    <link rel="dns-prefetch" href="//fonts.gstatic.com">
    <link href="https://fonts.googleapis.com/css?family=Nunito" rel="stylesheet" type="text/css">

    <!-- Styles -->
    <link href="{{ mix('css/app.css') }}" rel="stylesheet">
    <link href="{{ mix('css/admin.css') }}" rel="stylesheet">
</head>
<body>
    <div id="yuridik">
        <nav class="navbar navbar-expand-md navbar-dark bg-dark fixed-top shadow">
            <a class="navbar-brand col-sm-12 col-md-3 col-xl-2" href="{{ route('admin.dashboard') }}">
                {{ config('app.name') }}
            </a>

            <input class="form-control" type="search" placeholder="Search">

            <auth-menu :full-name="'{{ user()->fullName() }}'"></auth-menu>
        </nav>

        <div class="container-fluid">
            <div class="row">
                <nav class="sidebar col-md-3 col-xl-2 bg-light">
                    @include('admin.includes.sidebar')
                </nav>

                <main role="main" class="col-md-9 col-xl-10 ml-sm-auto">
                    @yield('admin.content')
                </main>
            </div>
        </div>
    </div>

    <!-- Scripts -->
    <script src="{{ mix('js/manifest.js') }}"></script>
    <script src="{{ mix('js/vendor.js') }}"></script>
    <script src="{{ mix('js/app.js') }}"></script>

    <!-- Build Yuridik Instance -->
    <script>
        window.Yuridik = new CreateYuridik();
    </script>

    <!-- Start Yuridik -->
    <script>
        Yuridik.liftOff();
    </script>
</body>
</html>
