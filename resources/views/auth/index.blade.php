<!DOCTYPE html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">

    <!-- CSRF Token -->
    <meta name="csrf-token" content="{{ csrf_token() }}">

    <!-- Title -->
    <title>{{ config('app.name') }}</title>

    <!-- Fonts -->
    <link rel="dns-prefetch" href="//fonts.gstatic.com">
    <link href="https://fonts.googleapis.com/css?family=Nunito" rel="stylesheet" type="text/css">

    <!-- Styles -->

    <link href="{{ mix('css/auth.css') }}" rel="stylesheet">
    <link href="{{ mix('assets/css/all.css') }}" rel="stylesheet">
</head>
<body>
    <div id="yuridik">
        <div class="auth-body">
            <div class="text-center">
                <h2 class="h2 font-weight-normal">
                    {{ config('app.name') }}
                </h2>

                <p class="text-muted">
                    {{ trans('Single authorization and registration') }}
                </p>
            </div>

            <auth-form>
                {{-- Auth Form --}}
            </auth-form>

            <p class="mt-5 mb-3 text-muted text-center">© {{ date('Y') }}</p>
        </div>
    </div>

    <!-- Scripts -->
    <script src="{{ mix('js/manifest.js') }}"></script>
    <script src="{{ mix('js/vendor.js') }}"></script>
    <script src="{{ mix('js/app.js') }}"></script>

    <!-- Build Yuridik Instance -->
    <script>
        window.Yuridik = new CreateYuridik();
    </script>

    <!-- Start Yuridik -->
    <script>
        Yuridik.liftOff();
    </script>
</body>
</html>
