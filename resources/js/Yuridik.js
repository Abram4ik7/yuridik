import Vue from 'vue';
import Toasted from 'vue-toasted';
import AsyncComputed from 'vue-async-computed';

import i18n from './lang';
import axios from './util/axios';

Vue.use(Toasted, {
    theme: 'toasted-primary',
    position: 'bottom-right',
    duration: 6000,
});

Vue.use(AsyncComputed);

export default class Yuridik {
    constructor () {
        this.bus = new Vue();
    }

    liftOff () {
        let _this = this;

        this.app = new Vue({

            el: '#yuridik',

            i18n,

            mounted() {
                _this.$on('info', message => {
                    this.$toasted.show(message, {
                        type: 'info',
                    });
                });

                _this.$on('success', message => {
                    this.$toasted.show(message, {
                        type: 'success',
                    });
                });

                _this.$on('error', message => {
                    this.$toasted.show(message, {
                        type: 'error',
                    });
                });

                _this.$on('token-expired', () => {
                    this.$toasted.show(_this.translate('Sorry, your session has expired.'), {
                        action: {
                            onClick: () => window.location.reload(),
                            text: _this.translate('Reload'),
                        },
                        duration: null,
                        type: 'error',
                    });
                });
            },
        });
    }

    request(options) {
        if (options !== undefined) {
            return axios(options);
        }
        return axios;
    }

    translate(key) {
        return i18n.t(key);
    }

    $on(...args) {
        this.bus.$on(...args);
    }

    $once(...args) {
        this.bus.$once(...args)
    }

    $off(...args) {
        this.bus.$off(...args);
    }

    $emit(...args) {
        this.bus.$emit(...args);
    }
}
