import Vue from 'vue';
import Yuridik from './Yuridik';
import './plugins';

Vue.config.productionTip = false;

import './components';

(function () {
    this.CreateYuridik = function () {
        return new Yuridik();
    }
}.call(window));
