import axios from 'axios';

const instance = axios.create();

instance.defaults.headers.common['X-Requested-With'] = 'XMLHttpRequest';
instance.defaults.headers.common['X-CSRF-TOKEN'] = document.head.querySelector('meta[name="csrf-token"]').content;

instance.interceptors.response.use(response => response, error => {
    const { status } = error.response;

    if (status >= 500) {
        Yuridik.$emit('error', error.response.data.message);
    }

    if (status === 401) {
        window.location.href = '/';
    }

    if (status === 403) {
        window.location.href = '/403';
    }

    if (status === 419) {
        Yuridik.$emit('token-expired');
    }

    return Promise.reject(error);
});

export default instance;
