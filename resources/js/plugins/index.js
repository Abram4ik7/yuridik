import './axios';
import './lodash';
import './jquery';
import './popper';
import './select2';
import 'bootstrap';
