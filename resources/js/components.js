import Vue from 'vue';

import Select2 from './components/Select2';
import QuestionsCreate from './components/QuestionsCreate';
import AuthForm from './components/AuthForm';
import AuthMenu from './components/Admin/AuthMenu';
import UsersForm from './components/Admin/UsersForm';

Vue.component('select2', Select2);
Vue.component('question-create', QuestionsCreate);
Vue.component('auth-form', AuthForm);
Vue.component('auth-menu', AuthMenu);
Vue.component('users-form', UsersForm);
