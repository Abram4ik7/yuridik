<?php

return [
    'home' => 'Bosh sahifa',
    'lawyers' => 'Huquqshunoslar',
    'lawyer' => 'Yuristlar',
    'posts' => 'Maqolalar',
    'active_lawyers' => 'Eng faol huquqshunoslar',
    'ye' => 'so`m',
    'users' => [
        'thanks' => 'minnatdorchilik'
    ],
    'more' => 'Batafsil',
    'search' => 'Izlash',
    'posts' => 'Maqolalar',
    'read_more' => 'Batafsil o`qish',
    'information' => 'Ma`lumot!',
    'not_posts' => 'Hozirda saytda maqolalar joylanmagan.',
    'question' => 'Savol',
    'questions' => [
        'title' => 'Savollar',
        'answers' => 'Javoblar',
        'all' => 'Barchasi',
        'paid' => 'Pullik',
        'free' => 'Bepul',
        'top' => 'Biriktirilgan',
        'price' => 'savol<br>narxi',
        'answer_reply' => 'Javob berish',
        'otmen' => 'bekor qilish',
        'speciality' => 'Huquq sohasi',
        'person' => 'Jismoniy shaxs',
        'legal_person' => 'Yuridik shaxs',
        'return_back' => 'Hamma savollarga qaytish',
        'not_answers' => 'Huquqshunoslar sizning savolingiz bilan shug`ullanishyabdi<br>Odatda javoblar 24 soat ichida ko`rib chiqiladi',
        // Юристы уже работают над вашим вопросом.<br>Обычно ответы поступают в течение суток.
        'not_answer_guest' => 'Hozircha savolga javob berilmagan!',
        'access_to_this_question_is_limited' => "Mazkur savolga kirish chegaralangan",
        'limited_reason' => "Mazkur sahifada foydalanuvchining shaxsiy maʼlumotlari mavjud. Arizani ko‘rish cheklanishiga quyidagi sabablar bo‘lishi mumkin:",
        'limited_reason_one' => "1. Ushbu savolga kirish faqat rasmiy tasdiqlangan yuristlarga ruxsat berilgan;",
        'limited_reason_two' => "2. Ushbu savol yopilgan va gonorar taqsimlanishi amalga oshirilgan;",
        'isNotReplyCounsel' => 'Ushbu savolga faqat rasmiy tasdiqlangan yuristlarga javob yozish ruxsat berilgan;',
        'success_reply' => 'Sizning javobingiz yuborildi. Javobingiz uchun raxmat!',
        'selected_files' => 'ta fayl tanlandi',
        'choose_file' => 'Fayl tanlash',
        'success_answered' => 'Javobingiz qabul qilindi. Javob uchun raxmat. Javobingizni ko`rish uchun sahidani yangilang!',
        'reply_answer' => 'Qo`shimcha savol berish',
        'gratitude' => 'Minnatdorchilik',
        'share' => 'Ulashing',
        'added_question' => 'Qo`shimcha qo`shish',
        'added_question_title' => 'Savolga qo`shimcha qo`shish', //Уточнение к вопросу
        'not_view_answers' => 'Bu savolni boshqa javoblarini siz kora olmaysiz!',
        'additional' => 'Savolga qo`shimcha:'

    ],

    'question_create' => [
        'next' => 'Davom ettirish',
        'who_man' => 'Savolni kim sifatida berasiz?',
        'oddiy_man' => 'Jismoniy shaxs',
        'yuridik_man' => 'Yuridik shaxs',
        'select' => '-- Tanlang --',
        'select_lang' => ' -- Tilni tanlang --',
        'title' => 'Sizning savolingiz *',
        'full_text' => 'Savol matni *',
        'create' => 'Savol berish',
        'service_id' => 'Savol turini tanlang',
        'user' => [
            'title' => 'Siz haqingizda maʼlumot',
            'first_name' => 'Ismingiz *',
            'last_name' => 'Familiyagiz *',
            'email' => 'Elektron adresingiz'
        ]

    ]

];