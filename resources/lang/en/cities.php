<?php

return [

    'no-region' => 'No region',
    'tashkent-city' => 'Tashkent city',
    'tashkent-region' => 'Tashkent region',
    'andijan' => 'Andijan',
    'bukhara' => 'Bukhara',
    'jizzakh' => 'Jizzakh',
    'karakalpakstan' => 'Karakalpakstan',
    'kashkadarya' => 'Kashkadarya',
    'navoi' => 'Navoi',
    'namangan' => 'Namangan',
    'samarkand' => 'Samarkand',
    'surkhandarya' => 'Surkhandarya',
    'syrdarya' => 'Syrdarya',
    'fergana' => 'Fergana',
    'khorezm' => 'Khorezm',

];
