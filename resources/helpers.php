<?php

use Illuminate\Routing\Router;
use Illuminate\Contracts\Auth\Factory as AuthFactory;

if (! function_exists('active')) {

    function active(string $name): bool
    {
        return app(Router::class)->is($name);
    }
}

if (! function_exists('user')) {

    function user()
    {
        return app(AuthFactory::class)->user();
    }
}
