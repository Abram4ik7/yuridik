$('.btn_pul').on('click',function(e){
    $('#bepul_tab').removeClass('tab_active')
    $('#pullik_tab').addClass('tab_active')

    $(this).removeClass('btn-default')
    $(this).addClass('btn-primary')

    $('.btn_bepul').removeClass('btn-primary')
    $('.btn_bepul').removeClass('btn-default')
})

$('.btn_bepul').on('click',function(e){
    $('#pullik_tab').removeClass('tab_active')
    $('#bepul_tab').addClass('tab_active')

    $(this).removeClass('btn-default')
    $(this).addClass('btn-primary')

    $('.btn_pul').removeClass('btn-primary')
    $('.btn_pul').removeClass('btn-default')
})




var swiper = new Swiper('.maqollar_slider', {
    slidesPerView: 3,
    spaceBetween: 50,
    navigation: {
        nextEl: '.swiper-button-next',
        prevEl: '.swiper-button-prev',
    },
    autoplay: true,
        // init: false,
        navigation: {
            nextEl: '.m_next',
            prevEl: '.m_prev',
        },
        breakpoints: {
            1024: {
                slidesPerView: 3,
                spaceBetween: 40,
            },
            768: {
                slidesPerView: 2.5,
                spaceBetween: 30,
            },
            640: {
                slidesPerView: 1,
                spaceBetween: 20,
            },
            320: {
                slidesPerView: 1,
                spaceBetween: 10,
            }
        }
    });

var swiper2 = new Swiper('.lawyers_slider', {
    slidesPerView: 4.5,
    spaceBetween: 25,
    autoplay: true,
    navigation: {
        nextEl: '.lw_next',
        prevEl: '.lw_prev',
    },
    breakpoints: {
        1024: {
            slidesPerView: 4,
            spaceBetween: 40,
        },
        768: {
            slidesPerView: 3,
            spaceBetween: 30,
        },
        640: {
            slidesPerView: 1.5,
            spaceBetween: 20,
        },
        320: {
            slidesPerView: 1.2,
            spaceBetween: 10,
        }
    }
});


(function($) {
        $('.accordion > li:eq(0) a').addClass('active').next().slideDown();

        $('.accordion a').click(function(j) {
            var dropDown = $(this).closest('li').find('p');

            $(this).closest('.accordion').find('p').not(dropDown).slideUp();

            if ($(this).hasClass('active')) {
                $(this).removeClass('active');
            } else {
                $(this).closest('.accordion').find('a.active').removeClass('active');
                $(this).addClass('active');
            }

            dropDown.stop(false, true).slideToggle();

            j.preventDefault();
        });
    })(jQuery);

    //Smooth Scrolling Using Navigation Menu
    $('a[href*="#"]').on('click', function(e){
        $('html,body').animate({
            scrollTop: $($(this).attr('href')).offset().top - 0
        },1000);
        e.preventDefault();
    });